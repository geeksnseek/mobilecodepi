/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Pidev.com;

import Entity.Produit;
import Entity.Transporteur;

/**
 *
 * @author ahmed
 */
public class Session {

    public static boolean verif = false;
    private static Transporteur transporteur;
    private static Produit produit;

    public static boolean isVerif() {
        return verif;
    }

    public static void setVerif(boolean verif) {
        Session.verif = verif;
    }

    public static Transporteur getTransporteur() {
        return transporteur;
    }

    public static void setTransporteur(Transporteur transporteur) {
        Session.transporteur = transporteur;
    }

    public static Produit getProduit() {
        return produit;
    }

    public static void setProduit(Produit produit) {
        Session.produit = produit;
    }

}
