/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Event;
import Entity.Participant;
import GuiForm.EvenementMenuForm;
import GuiForm.ShowMyEventsForm;
import static GuiForm.ShowMyEventsForm.participerbtn;
import static GuiForm.SignInForm.res;
import GuiForm.addEventForm;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.InteractionDialog;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.JSONParser;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.social.FacebookConnect;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author debba
 */
public class EventServices {

    ////Ajout d'un evenement
    public void ajoutEvent(Event ta) {
        ConnectionRequest con = new ConnectionRequest();
        System.out.println(MembreService.user.getId());
        String date = addEventForm.picker.getText();

        String Url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/newEvent/" + MembreService.user.getId()
                + "?nom=" + ta.getNom()
                + "&type=" + ta.getType()
                + "&lieu=" + ta.getLieu()
                + "&description=" + ta.getDescription()
                + "&heure=" + ta.getHeure()
                + "&nomImage=" + ta.getNomImage()
                + "&date=" + date;
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
            Dialog.show("Succés", "Votre Evénement est ajouté, merci de participer avec nous", "ok", null);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    ////Affichage d'un evenement

    public ArrayList<Event> getListAllEvents() {
        ArrayList<Event> listTasks = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/allEvents2/"+MembreService.user.getId());

        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(tasks);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> obj : list) {
                        Event task = new Event();
                        task.setId((int) Float.parseFloat(obj.get("id").toString()));
                        task.setNom(obj.get("nom").toString());

//                        String DateS = obj.get("date").toString().substring(obj.get("date").toString().indexOf("timestamp") + 10, obj.get("date").toString().indexOf("timestamp") + 21);
//                        Date currentTime = new Date(Double.valueOf(DateS).longValue() * 1000);
//                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//                        dateString = formatter.format(currentTime);
//                        System.out.println(dateString);
//                        
//                        task.setDate(currentTime);
                        //task.setLieu(obj.get("lieu").toString());
                        //task.setDate();
                        task.setType(obj.get("type").toString());
                        task.setLieu(obj.get("lieu").toString());
                        task.setDescription(obj.get("description").toString());
                        task.setHeure(obj.get("heure").toString());
                        task.setDate(obj.get("date").toString());
                        task.setNomImage(obj.get("nomImage").toString());
                        listTasks.add(task);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTasks;
    }

/////Suppression
    public void supprimerEvent(Event ta, Resources res) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/RemoveEvent/" + ta.getId();
        con.setUrl(url);
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
            Dialog.show("Succés", "Event supprimé", "ok", null);

            ShowMyEventsForm a = new ShowMyEventsForm(res);
            a.show();
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    //////recherche 

    public ArrayList<Event> ChercherEvent(String d) {
        ArrayList<Event> listEvent = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/FindEvent/" + d);
        con.addResponseListener((NetworkEvent evttt) -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                System.out.println("tttt5tt"+tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                for (Map<String, Object> obj : list) {
                    Event task = new Event();
                    task.setNomImage(obj.get("nomImage").toString());
                    float id = Float.parseFloat(obj.get("id").toString());
                    //float idm = Float.parseFloat(obj.get(7).toString());
                    task.setId((int) id);
                    // task.setIdm(7);
                    task.setDescription(obj.get("description").toString());
                    task.setNom(obj.get("nom").toString());
                    task.setType(obj.get("type").toString());
                    task.setLieu(obj.get("lieu").toString());
                    task.setHeure(obj.get("heure").toString());
                    task.setDate(obj.get("date").toString());
                    // float nbp = Float.parseFloat(obj.get("nbreticket").toString());
                    //task.setNbreticket((int) nbp);
//                  task.setDate((Date) obj.get("date"));
                    listEvent.add(task);
                }
            } catch (IOException ex) {
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvent;
    }
    ////Modification

    public void modifierEvent(Event ta, Resources res) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/updateEvent/" + ta.getId()+"/"+MembreService.user.getId()
                + "?nom=" + ta.getNom()
                + "&lieu=" + ta.getLieu()
                + "&type=" + ta.getType()
                + "&description=" + ta.getDescription()
                + "&heure=" + ta.getHeure();
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
         Dialog.show("Succés", "Event modifié", "ok", null);
            ShowMyEventsForm a = new ShowMyEventsForm(res);
            a.show();

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    ///////Participation

    public void participerEvent(Event ta) {
        ConnectionRequest con = new ConnectionRequest();
        System.out.println(MembreService.user.getId());

        String Url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/add_Participant/" + ta.getId() + "/" + MembreService.user.getId();

        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
            Dialog.show("Succés", "Votre demande est validée", "ok", null);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }

////Affichage de mes  evenement
    public ArrayList<Event> getListMyEvents() {
        ArrayList<Event> listTasks = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/MyEvents/" + MembreService.user.getId());

        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(tasks);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> obj : list) {
                        Event task = new Event();
                        task.setId((int) Float.parseFloat(obj.get("id").toString()));
                        task.setNom(obj.get("nom").toString());
                        task.setType(obj.get("type").toString());
                        task.setLieu(obj.get("lieu").toString());
                        task.setDescription(obj.get("description").toString());
                        task.setHeure(obj.get("heure").toString());
                        task.setNomImage(obj.get("nomImage").toString());

                        listTasks.add(task);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTasks;
    }
    ////Affichage de ma participation

    public ArrayList<Event> getListMyParticipation() {
        ArrayList<Event> listTasks = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/MyParticipation/" + MembreService.user.getId());

        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(tasks);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> obj : list) {
                        Event task = new Event();
                        task.setId((int) Float.parseFloat(obj.get("id").toString()));
                        task.setNom(obj.get("nom").toString());

                        task.setType(obj.get("type").toString());
                        task.setLieu(obj.get("lieu").toString());
                        task.setDescription(obj.get("description").toString());
                        task.setHeure(obj.get("heure").toString());
                        task.setNomImage(obj.get("nomImage").toString());
                        listTasks.add(task);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listTasks;
    }
        //////rechercher Participant 

    public  static ArrayList<Participant> ChercherParticipant(int idevent,int idu) {
        ArrayList<Participant> listParticipant = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/findParticipant/"+idevent+"/"+MembreService.user.getId());
        con.addResponseListener((NetworkEvent evttt) -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                System.out.println(tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                System.out.println(list);
                for (Map<String, Object> obj : list) {
                    Participant task = new Participant();
                    task.setAvis(obj.get("avis").toString());
                    float idP = Float.parseFloat(obj.get("idp").toString());
                    task.setIdp((int) idP); 
                    task.setEtat(obj.get("etat").toString());  
                    listParticipant.add(task);
                    System.out.println("mella hkeya");
                }
            } catch (IOException ex) {
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        System.out.println(listParticipant);
        return listParticipant;
    }
    ////Donner mon avis

    public void DonnerAvisEvent(Event ta,String k) {
        ConnectionRequest con = new ConnectionRequest();
        System.out.println(MembreService.user.getId());
   int     idu=MembreService.user.getId();
   int idevent=ta.getId();
     EventServices ES=new EventServices();
   Participant listp=ES.ChercherParticipant(idevent,idu).get(0);
    System.out.println(listp);
     int j=0;   

        String Url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/AvisEvent/"+idu+"/"+idevent
                + "?avis=" + k;
   
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);
            Dialog.show("Succés", "Avis enregistré! Merci d'avoir partager votre avis avec nous ", "ok", null);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
        ////// 
    public ArrayList<Float> CodeCadeauEvent(String code) {
        ArrayList<Float> listEvent = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/CodeCadeaux/" + MembreService.user.getId()+"/"+code);
        con.addResponseListener((NetworkEvent evttt) -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                System.out.println("opppp"+tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                for (Map<String, Object> obj : list) {
                  
                    float id = Float.parseFloat(obj.get("info").toString());
                    System.out.println(id);

                    // float nbp = Float.parseFloat(obj.get("nbreticket").toString());
                    //task.setNbreticket((int) nbp);
//                  task.setDate((Date) obj.get("date"));
                    listEvent.add(id);
                }
            } catch (IOException ex) {
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
       
        return listEvent;
    }
        public ArrayList<Float> ChercherNombre() {
        ArrayList<Float> listEvent = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/findnb/"+ MembreService.user.getId());
        con.addResponseListener((NetworkEvent evttt) -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                System.out.println("opppp"+tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                for (Map<String, Object> obj : list) {
                  
                    float id = Float.parseFloat(obj.get("info").toString());
                    System.out.println(id);

                    // float nbp = Float.parseFloat(obj.get("nbreticket").toString());
                    //task.setNbreticket((int) nbp);
//                  task.setDate((Date) obj.get("date"));
                    listEvent.add(id);
                }
            } catch (IOException ex) {
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvent;
    }
       public ArrayList<Float> VerifCode(String criteria) {
        ArrayList<Float> listEvent = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/VerifCode/"+ MembreService.user.getId()+"/"+criteria);
        con.addResponseListener((NetworkEvent evttt) -> {
            JSONParser jsonp = new JSONParser();
            try {
                Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                System.out.println("opppp"+tasks);
                List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                for (Map<String, Object> obj : list) {
                  
                    float id = Float.parseFloat(obj.get("verifcode").toString());
                    System.out.println(id);

                    listEvent.add(id);
                }
            } catch (IOException ex) {
            }

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvent;
    }

}
