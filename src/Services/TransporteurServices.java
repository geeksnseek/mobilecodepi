/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Transporteur;
import com.Pidev.com.Session;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author Ahmed
 */
public class TransporteurServices {

    private Transporteur transporteur = new Transporteur();

    public void Login(int cin) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/Bouhmid/web/app_dev.php/mobile/login?cin=" + cin;
        con.setUrl(url);
        con.addResponseListener((evt) -> {
            String response = new String(con.getResponseData());
            if (!response.trim().contains("NotExist")) {
                try {
                    JSONParser j = new JSONParser();
                    Map<String, Object> userLog = j.parseJSON(new CharArrayReader(response.toCharArray()));
                    transporteur.setId(Float.valueOf(userLog.get("id").toString()).intValue());
                    transporteur.setCin(Float.valueOf(userLog.get("cin").toString()).intValue());
                    transporteur.setTelephone(Float.valueOf(userLog.get("telephone").toString()).intValue());
                    transporteur.setNom(userLog.get("nom").toString());
                    transporteur.setPrenom(userLog.get("prenom").toString());
                    transporteur.setPermit(userLog.get("permit").toString());
                    Session.setVerif(true);
                    Session.setTransporteur(transporteur);
                } catch (IOException ex) {
                }
            } else {
                Dialog.show("Conf", "CIN Invalide", "ok", null);
                Session.setVerif(false);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    
    

}
