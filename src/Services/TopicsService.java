/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Categorie;
import Entity.Topics;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Label;
import com.codename1.util.StringUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jalel
 */
public class TopicsService {

    public List<Topics> getAllTopics(String wi) {
        String url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/show_topic";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        //  con.setHttpMethod("GET");
// rech
        con.setUrl(url);
        if (wi != null) {
            if (!wi.isEmpty()) {
                con.setPost(true);
                con.addArgument("wi", wi);
            }
        }
        // 
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Topics> le = new ArrayList<>();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");

                for (Map<String, Object> r : res) {

                    Topics e = new Topics();
                    e.setTopic_id(((Double) r.get("topicId")).intValue());
                    e.setTopic_title((String) r.get("topicTitle"));
                    e.setPhoto((String) r.get("photo"));
                    e.setTopic_subject((String) r.get("topicSubject"));

                    //  e.setDispo((int) r.get("dispo"));
                    /*   User u = new User();
                    u.setId(((Double) uu.get("id")).intValue());
                  
                 e.set(u);
                     */
                    le.add(e);
                }

            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le;
    }

    public boolean ajouterTopic(Topics o) {

        String url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/aj_topic";
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
         Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
     
        // post data
        // con.addArgument("iduser",o.getIduser().getId().toString());
        con.addArgument("topicTitle", o.getTitle());
        con.addArgument("topicSubject", o.getTopic_subject());
      //  con.addArgument("topicCat", ""+o.getTopic_cat());
        con.addArgument("photo", o.getPhoto());

        // send request
        NetworkManager.getInstance().addToQueueAndWait(con);
        String str = new String(con.getResponseData());
        // parsing data
        if (con.getResponseCode() != 200) {
            System.out.println(str);
            return false;
        } else {
            System.out.println(str);
            return true;
        }

    }

    public void SupprimerOffre(int id) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/suupp_topic/" + id;
        con.setUrl(Url);

        NetworkManager.getInstance().addToQueueAndWait(con);
    }

    public boolean ModiferTopic(int id, Topics o) {

        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/mood_topic/" + id;
        con.setUrl(Url);
        con.setPost(true);
        // post data
        // con.addArgument("iduser",o.getIduser().getId().toString());
        con.addArgument("topicTitle", o.getTitle());
        con.addArgument("topicSubject", o.getTopic_subject());
        // send request
        NetworkManager.getInstance().addToQueueAndWait(con);
        String str = new String(con.getResponseData());
        // parsing data
        if (con.getResponseCode() != 200) {
            System.out.println(str);
            return false;
        } else {
            System.out.println(str);
            return true;
        }

    }

    public static int getCatID(String n) {
        String url = "http://localhost/SymfonyPi/web/app_dev.php/api/topics/aff_catname/" + n;
        url = StringUtil.replaceAll(url, " ", "%20");
        ConnectionRequest con = new ConnectionRequest();
        con.setHttpMethod("GET");
        con.setUrl(url);
        Label lb = new Label();
        con.addResponseListener(evvvv -> {
            String reponse = new String(con.getResponseData());
            lb.setText(reponse);
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        List<Categorie> le = new ArrayList<>();
        if (!lb.getText().equals("{}")) {
            try {
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(con.getResponseData()), "UTF-8"));
                List<Map<String, Object>> res = (List<Map<String, Object>>) result.get("root");
                for (Map<String, Object> r : res) {
                    Categorie e1 = new Categorie();
                    e1.setCat_id(((Double) r.get("catId")).intValue());
//                    e.setIduser((int) r.get("iduser"));
                    le.add(e1);
                }
            } catch (UnsupportedEncodingException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return le.get(0).getCat_id();
    }

}
