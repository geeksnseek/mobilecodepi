/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Mession;
import com.Pidev.com.Session;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ahmed
 */
public class MissionService {

    public ArrayList<Mession> parseListTaskJson(String json) {

        ArrayList<Mession> listMess = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");

            //Parcourir la liste des tâches Json
            for (Map<String, Object> obj : list) {
                //Création des tâches et récupération de leurs données
                Mession m = new Mession();
                float id = Float.parseFloat(obj.get("id").toString());
                m.setId((int) id);
                m.setId_transport(Float.valueOf(obj.get("idprod").toString()).intValue());
                m.setId_prod(Float.valueOf(obj.get("idtrans").toString()).intValue());
                m.setQte(Float.valueOf(obj.get("qte").toString()).intValue());
                m.setNomtrans(obj.get("nomtrans").toString());
                m.setNomprod(obj.get("nomprod").toString());
                listMess.add(m);
            }
        } catch (IOException ex) {
        }
        return listMess;

    }
    ArrayList<Mession> listMess = new ArrayList<>();

    public ArrayList<Mession> getListMission() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Bouhmid/web/app_dev.php/mobile/missions/"+Session.getTransporteur().getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) { 
                MissionService ser = new MissionService();
                listMess = ser.parseListTaskJson(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listMess;
    }

    public void ajoute(int id,int prod) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/Bouhmid/web/app_dev.php/mobile/ajoute?id=" + id + "&prod=" + prod;
        con.setUrl(url);
        con.addResponseListener((evt) -> {
            String response = new String(con.getResponseData());
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public void delete(int id) {
        ConnectionRequest con = new ConnectionRequest();
        String url = "http://localhost/Bouhmid/web/app_dev.php/mobile/delete/" + id;
        con.setUrl(url);
        con.addResponseListener((evt) -> {
            String response = new String(con.getResponseData());
        });
        NetworkManager.getInstance().addToQueue(con);
    }
}
