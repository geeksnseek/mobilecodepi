/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Stock;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author brini
 */
public class StockService
{
    ArrayList<Stock> list;
    
    
    
   public ArrayList<Stock> getList1()
   {
       list = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/SymfonyPi/web/app_dev.php/mobile/listStock");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    //listTasks = getListTask(new String(con.getResponseData()));
                    JSONParser jsonp = new JSONParser();

              
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> Stock = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println("roooooot:" + Stock.get("root"));

                    List<Map<String, Object>> listStock = (List<Map<String, Object>>) Stock.get("root");
           
                    for (Map<String, Object> obj : listStock) {
                 
                        Stock ls = new Stock();
                        
                        int id = (int) Float.parseFloat(obj.get("idCat").toString());
                        double prix = Double.parseDouble(obj.get("prixuni").toString());
                        int quantite = (int) Float.parseFloat(obj.get("quantitedispo").toString());
                        String description = obj.get("description").toString();
                        String image =obj.get("nomimage").toString();
                       ls.setIdCat(id);
                       ls.setDescription(description);
                       ls.setPrixuni(prix);
                       ls.setQuantitedispo(quantite);
                       ls.setNomimage(image);
                    
                       
                       list.add(ls);

                

                    }
              
                } catch (IOException ex) {

                }

            }
        });
     
        NetworkManager.getInstance().addToQueueAndWait(con);
        System.out.println(list);
        return list;
    } 
}
