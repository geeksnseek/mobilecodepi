/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Produit;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ahmed
 */
public class ProduitService {

    public ArrayList<Produit> parseListTaskJson(String json) {

        ArrayList<Produit> listProds = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");

            //Parcourir la liste des tâches Json
            for (Map<String, Object> obj : list) {
                //Création des tâches et récupération de leurs données
                Produit p = new Produit();
                float id = Float.parseFloat(obj.get("id").toString());
                p.setId((int) id);
                p.setNom(obj.get("nom").toString());
                p.setStatus(obj.get("status").toString());
                p.setGte(Float.valueOf(obj.get("quantite").toString()));
                p.setImg(obj.get("image").toString());
                p.setType(obj.get("type").toString());
                p.setDesc(obj.get("descriptipn").toString());
                p.setDat(obj.get("dat").toString());
                listProds.add(p);

            }

        } catch (IOException ex) {
        }
        return listProds;

    }
    ArrayList<Produit> listProds = new ArrayList<>();

    public ArrayList<Produit> getListMessions() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/Bouhmid/web/app_dev.php/mobile/produit");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ProduitService ser = new ProduitService();
                listProds = ser.parseListTaskJson(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listProds;
    }

}
