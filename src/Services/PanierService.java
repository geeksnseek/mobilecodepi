/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Commande;
import Entity.Panier;
import Entity.Stock;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author brini
 */
public class PanierService
{
        ArrayList<Panier> listLigneCommande ;
        ArrayList<Stock> listProduit;
        ArrayList<Commande> listcomm ;
        
        
    public void ajouterlc(int ids , double qte)
    {
        ConnectionRequest con = new ConnectionRequest();
        
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/ajouterlc/" + MembreService.user.getId() + "/" +ids+ "/" +qte;
        con.setUrl(Url);
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
       public void supplc(int id)
       {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/suppLc/" + MembreService.user.getId()+"/" +id ;
        con.setUrl(Url); 
       NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
      public void modiflc(int id ,float qte) 
      {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/modifLc/"+id+"/"+qte ;
        con.setUrl(Url); 
       NetworkManager.getInstance().addToQueueAndWait(con);
    }
         
       public void suppCommande()
       {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/SuppCom/" + MembreService.user.getId();
        con.setUrl(Url); 
       NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
    public  ArrayList<Panier> affichepanier()
    {
             listLigneCommande  = new ArrayList<Panier>();
        ConnectionRequest con = new ConnectionRequest();
        
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/AllLc/" + MembreService.user.getId() ;
        con.setUrl(Url);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
      
                    JSONParser jsonp = new JSONParser();

              
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> lc = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                    List<Map<String, Object>> list = (List<Map<String, Object>>)lc.get("root");
           
                    for (Map<String, Object> obj : list) {
                 
                       
                        Panier p = new Panier();
                                        
              
                        int idCat = (int) Float.parseFloat(obj.get("idCat").toString());
                        int id = (int) Float.parseFloat(obj.get("id").toString());
                        int idcmd = (int) Float.parseFloat(obj.get("idCmd").toString());
                        float  prix= Float.parseFloat(obj.get("prix").toString());
                        float  quantite = Float.parseFloat(obj.get("quantite").toString());
                        float  total = Float.parseFloat(obj.get("total").toString());
                        String desc = obj.get("description").toString();
                        String nomimage = obj.get("nomimage").toString();
                        float  quantitedisp = Float.parseFloat(obj.get("quantitedispo").toString());
                        Stock s = new Stock();
                        s.setIdCat(idCat);
                        s.setDescription(desc);
                        s.setNomimage(nomimage);
                        s.setQuantitedispo(quantitedisp);
                        System.out.println(s);
                        p.setId(id);
                        p.setIdcommande(idcmd);
                        p.setIdStock(s);
                 
                        p.setQuantite(quantite);
                        p.setPrix(prix);
                        p.setTotal(total);

                     
                        
                        

                        listLigneCommande.add(p);
                         

                

                    }
              
                } catch (IOException ex) {

                }

            }
        });
     
        NetworkManager.getInstance().addToQueueAndWait(con);

        return    listLigneCommande;
    }
    
    
    public void validerCom()
    {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/valider/" + MembreService.user.getId();
        con.setUrl(Url); 
       NetworkManager.getInstance().addToQueueAndWait(con);
    }    
    
    public ArrayList<Commande> mesCom()
            
    { 
        listcomm  = new ArrayList<Commande>();
        
        
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/symfonypi/web/app_dev.php/mobile/Mescom/" + MembreService.user.getId();
          con.setUrl(Url);
     
              con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
      
                    JSONParser jsonp = new JSONParser();

              
                    //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> lc = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));

                    List<Map<String, Object>> list = (List<Map<String, Object>>)lc.get("root");
                    System.out.println("hhhhh");
                    for (Map<String, Object> obj : list) {
                 
                       
                     Commande c = new Commande();
                                        
              
                   
                        float  total = Float.parseFloat(obj.get("total").toString());
                        String date = obj.get("date").toString();
                        String dateexp = obj.get("dateexp").toString();
                            System.out.println("hethii"+dateexp);
                        String payement = obj.get("payement").toString();
                    
                 
                 
                    

                     c.setDate(date);
                     c.setDateexp(dateexp);
                     c.setTotal(total);
                     c.setPayement(payement);
                        
                        
                    listcomm.add(c);
           
                         

                

                    }
              
                } catch (IOException ex) {

                }

            }
        });
     
        NetworkManager.getInstance().addToQueueAndWait(con);

        return listcomm ;
        
    }
}
