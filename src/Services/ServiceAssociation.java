/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entity.Association;
import Entity.Demande;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author med
 */
public class ServiceAssociation {

    String a = "";

    public void ajoutAsso(Association a) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = "http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/newA?nom=" + a.getNomAssociation() + "&adresse=" + a.getAdresse() + "&capital=" + a.getCapital() + "&membre=" + a.getChefId() + "&image=" + a.getNomImage();// création de l'URL
        //String Url = "http://localhost:3309/symfony-api/web/app_dev.php/api/tasks/new?name=task1&status=0";
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }
    public void updateAsso(Association a) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = "http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/updateA?nom=" + a.getNomAssociation() + "&adresse=" + a.getAdresse() + "&capital=" + a.getCapital() + "&ida=" + a.getIdAssociation() ;// création de l'URL
        //String Url = "http://localhost:3309/symfony-api/web/app_dev.php/api/tasks/new?name=task1&status=0";
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }

    public void deleteAsso(Association a) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = "http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/deleteA?ida=" + a.getIdAssociation();
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }

    public ArrayList<Association> getList2() {
        ArrayList<Association> listAssos = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/listA");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> Asso = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println("msg" + Asso);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) Asso.get("root");
                    for (Map<String, Object> obj : list) {
                        Association a = new Association();
                        float id = Float.parseFloat(obj.get("idAssociation").toString());
                        float cpt = Float.parseFloat(obj.get("capital").toString());
                        a.setIdAssociation((int) id);
                        a.setCapital((int) cpt);
                        System.out.println(a.getIdAssociation());
                        a.setNomAssociation(obj.get("nomAssociation").toString());
                        System.out.println(a.getNomAssociation());
                        a.setAdresse(obj.get("adresse").toString());
                        a.setDateCreation(obj.get("dateCreation").toString());
                        a.setNomImage(obj.get("nomImage").toString());
                        //int i= Integer.parseInt(obj.get("idUser").toString());
                        //a.setId_user(i);
                        System.out.println(a);

                        listAssos.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listAssos;
    }

    public ArrayList<Association> getListadh() {
        ArrayList<Association> listAssos = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/listAd?membre=" + MembreService.user.getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> Asso = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println("msg" + Asso);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) Asso.get("root");
                    for (Map<String, Object> obj : list) {
                        Association a = new Association();
                        float id = Float.parseFloat(obj.get("idAssociation").toString());

                        a.setIdAssociation((int) id);
                        System.out.println(a.getIdAssociation());
                        a.setNomAssociation(obj.get("nomAssociation").toString());
                        System.out.println(a.getNomAssociation());
                        a.setAdresse(obj.get("adresse").toString());
                        a.setDateCreation(obj.get("dateCreation").toString());
                        a.setNomImage(obj.get("nomImage").toString());
                        //int i= Integer.parseInt(obj.get("idUser").toString());
                        //a.setId_user(i);
                        System.out.println(a);

                        listAssos.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listAssos;
    }

    public ArrayList<Association> getListMyAssociations() {
        ArrayList<Association> listAssos = new ArrayList<>();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/listMyA?chef=" + MembreService.user.getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                JSONParser jsonp = new JSONParser();

                try {
                    Map<String, Object> Asso = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println("msg" + Asso);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) Asso.get("root");
                    for (Map<String, Object> obj : list) {
                        Association a = new Association();
                        float id = Float.parseFloat(obj.get("idAssociation").toString());

                        a.setIdAssociation((int) id);
                        System.out.println(a.getIdAssociation());
                        a.setNomAssociation(obj.get("nomAssociation").toString());
                        System.out.println(a.getNomAssociation());
                        a.setAdresse(obj.get("adresse").toString());
                        a.setDateCreation(obj.get("dateCreation").toString());
                        a.setNomImage(obj.get("nomImage").toString());
                        //int i= Integer.parseInt(obj.get("idUser").toString());
                        //a.setId_user(i);
                        System.out.println(a);

                        listAssos.add(a);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listAssos;
    }

    public String getButtonName(int ida) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/btName?membre=" + MembreService.user.getId() + "&idasso=" + ida);
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                JSONParser jsonp = new JSONParser();
                try {
                    Map<String, Object> Asso = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println("msg" + Asso);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) Asso.get("root");
                    for (Map<String, Object> obj : list) {
                        a = obj.get("btname").toString();
                        System.out.println(a);
                    }
                } catch (IOException ex) {
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return a;
    }

    public void ajoutAdheration(Demande d) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = "http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/newD?membre=" + d.getIdm() + "&idasso=" + d.getIda();
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }
    
    public void ajoutVote(int ida,float stars,String desc) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = "http://localhost:3309/SymfonyPi/web/app_dev.php/mobile/rating?membre="+MembreService.user.getId()+"&idasso="+ida+"&star="+stars+"&desc="+desc;
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion
        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }

}
