/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class Actualite {
    private int id ;
    private String titre;
    private String description;
    private Date  date;
    private String pictures;
    private int archivestatut =1;
    private String categories;
    private int user;
    private int nbrelike =0;

    public Actualite(int id, String titre, String description,Date date, String pictures, String categories, int user) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.date = date;
        this.pictures = pictures;
        this.categories = categories;
        this.user = user;
    }

    public Actualite() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public int getArchivestatut() {
        return archivestatut;
    }

    public void setArchivestatut(int archivestatut) {
        this.archivestatut = archivestatut;
    }

    public String  getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getNbrelike() {
        return nbrelike;
    }

    public void setNbrelike(int nbrelike) {
        this.nbrelike = nbrelike;
    }

    @Override
    public String toString() {
        return "Actualite{" + "id=" + id + ", titre=" + titre + ", description=" + description + ", date=" + date + ", pictures=" + pictures + ", archivestatut=" + archivestatut + ", categories=" + categories + ", user=" + user + ", nbrelike=" + nbrelike + '}';
    }
    
    
}
