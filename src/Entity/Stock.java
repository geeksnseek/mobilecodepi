/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author brini
 */
public class Stock 
{
    private int idCat ;
    private double quantitedispo ;
    private String description; 
    private double prixuni ;
    private String nomimage ;

    public Stock() {
    }

    public Stock(int idCat, String description) {
        this.idCat = idCat;
        this.description = description;
    }
    

    public Stock(int idCat, double quantitedispo, String description, double prixuni, String nomimage) {
        this.idCat = idCat;
        this.quantitedispo = quantitedispo;
        this.description = description;
        this.prixuni = prixuni;
        this.nomimage = nomimage;
    }

    public Stock(int idCat, double quantitedispo, String description, double prixuni) {
        this.idCat = idCat;
        this.quantitedispo = quantitedispo;
        this.description = description;
        this.prixuni = prixuni;
    }

    public int getIdCat() {
        return idCat;
    }

    public void setIdCat(int idCat) {
        this.idCat = idCat;
    }

    public double getQuantitedispo() {
        return quantitedispo;
    }

    public void setQuantitedispo(double quantitedispo) {
        this.quantitedispo = quantitedispo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrixuni() {
        return prixuni;
    }

    public void setPrixuni(double prixuni) {
        this.prixuni = prixuni;
    }

    public String getNomimage() {
        return nomimage;
    }

    public void setNomimage(String nomimage) {
        this.nomimage = nomimage;
    }

    @Override
    public String toString() {
        return "Stock{" + ", quantitedispo=" + quantitedispo + ", description=" + description + ", prixuni=" + prixuni + ", nomimage=" + nomimage + '}';
    }
    
    
    
}
