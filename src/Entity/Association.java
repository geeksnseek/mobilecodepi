/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.InputStream;
import java.util.Date;


/**
 *
 * @author med
 */
public class Association {
    private int idAssociation;
    private String nomAssociation;
    private String dateCreation;
    private String adresse;
    private double capital;
    private int chefId;
//    private InputStream image;
    private String nomImage;

    public Association(int idAssociation, String nomAssociation, String dateCreation, String adresse, double capital, int chefId, String nomImage) {
        this.idAssociation = idAssociation;
        this.nomAssociation = nomAssociation;
        this.dateCreation = dateCreation;
        this.adresse = adresse;
        this.capital = capital;
        this.chefId = chefId;
        this.nomImage = nomImage;
    }

    public Association(int idAssociation, String nomAssociation, String adresse, double capital, int chefId,String img) {
        this.idAssociation = idAssociation;
        this.nomAssociation = nomAssociation;
        this.adresse = adresse;
        this.capital = capital;
        this.chefId = chefId;
        this.nomImage = img;
    }

    public Association() {
    }
    

    public int getIdAssociation() {
        return idAssociation;
    }

    public void setIdAssociation(int idAssociation) {
        this.idAssociation = idAssociation;
    }

    public String getNomAssociation() {
        return nomAssociation;
    }

    public void setNomAssociation(String nomAssociation) {
        this.nomAssociation = nomAssociation;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public int getChefId() {
        return chefId;
    }

    public void setChefId(int chefId) {
        this.chefId = chefId;
    }

    public String getNomImage() {
        return nomImage;
    }

    public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }
    
    
    
    
}
