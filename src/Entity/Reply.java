/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author nawre
 */
public class Reply {

    private int id;
    private int user_id;
    private String Contenu;
    private String Date_Creation;
    private int sujet_id;
    private int likes;

    public Reply() {
    }
    

    public Reply(int user_id, String Contenu, String Date_Creation, int sujet_id, int likes) {
        this.user_id = user_id;
        this.Contenu = Contenu;
        this.Date_Creation = Date_Creation;
        this.sujet_id = sujet_id;
        this.likes = likes;
    }

    public Reply(int id, int user_id, String Contenu, String Date_Creation, int sujet_id, int likes) {
        this.id = id;
        this.user_id = user_id;
        this.Contenu = Contenu;
        this.Date_Creation = Date_Creation;
        this.sujet_id = sujet_id;
        this.likes = likes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getContenu() {
        return Contenu;
    }

    public void setContenu(String Contenu) {
        this.Contenu = Contenu;
    }

    public String getDate_Creation() {
        return Date_Creation;
    }

    public void setDate_Creation(String Date_Creation) {
        this.Date_Creation = Date_Creation;
    }

    public int getSujet_id() {
        return sujet_id;
    }

    public void setSujet_id(int sujet_id) {
        this.sujet_id = sujet_id;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "Reply{" + "id=" + id + ", user_id=" + user_id + ", Contenu=" + Contenu + ", Date_Creation=" + Date_Creation + ", sujet_id=" + sujet_id + ", likes=" + likes + '}';
    }
    

}
