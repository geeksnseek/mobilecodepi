/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;


/**
 *
 * @author brini
 */
public class Commande 
{
    private int idCmd ;
    private int id_user ;
    private String etat ;
    private String payement ;
    private String point ;
    private float  total ;
    private String date ;
    private String dateexp ;

    public Commande(int idCmd, int id_user, String etat, float total) {
        this.idCmd = idCmd;
        this.id_user = id_user;
        this.etat = etat;
        this.total = total;
 
    }

    @Override
    public String toString() {
        return "Commande{" + "idCmd=" + idCmd + ", id_user=" + id_user + ", etat=" + etat + ", payement=" + payement + ", point=" + point + ", total=" + total + '}';
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateexp() {
        return dateexp;
    }

    public void setDateexp(String dateexp) {
        this.dateexp = dateexp;
    }

    public Commande() {
    }

    
    
    
    
    
    
    public int getIdCmd() {
        return idCmd;
    }

    public void setIdCmd(int idCmd) {
        this.idCmd = idCmd;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getPayement() {
        return payement;
    }

    public void setPayement(String payement) {
        this.payement = payement;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }


    
    
 
}
