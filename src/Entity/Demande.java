/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author med
 */
public class Demande {
    private int id;
    private String etat;
    private int ida;
    private int idm;

    public Demande() {
    }

    public Demande(int id, String etat, int ida, int idm) {
        this.id = id;
        this.etat = etat;
        this.ida = ida;
        this.idm = idm;
    }

    public Demande(int ida, int idm) {
        this.ida = ida;
        this.idm = idm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getIda() {
        return ida;
    }

    public void setIda(int ida) {
        this.ida = ida;
    }

    public int getIdm() {
        return idm;
    }

    public void setIdm(int idm) {
        this.idm = idm;
    }
     
    
}
