/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author brini
 */
public class Panier 
{
    private int id ;
    private float quantite ;
    private float prix ;
    private float total ;
    private Stock idStock ;
    private int idcommande ;
    

    public Panier() {
    }
    

    public Panier(int id, float quantite, float prix, float total, Stock idStock, int idcommande) 
    {
        this.id = id;
        this.quantite = quantite;
        this.prix = prix;
        this.total = total;
        this.idStock = idStock;
        this.idcommande = idcommande;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getQuantite() {
        return quantite;
    }

    public void setQuantite(float quantite) {
        this.quantite = quantite;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Stock getIdStock() {
        return idStock;
    }

    public void setIdStock(Stock  idStock) {
        this.idStock = idStock;
    }

    public int getIdcommande() {
        return idcommande;
    }

    public void setIdcommande(int idcommande) {
        this.idcommande = idcommande;
    }

    @Override
    public String toString() {
        return "Panier{" + "id=" + id + ", quantite=" + quantite + ", prix=" + prix + ", total=" + total + ", idStock=" + idStock + ", idcommande=" + idcommande + '}';
    }
    
    
}
