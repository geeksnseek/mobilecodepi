/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import com.codename1.ui.Form;
import java.util.Date;

/**
 *
 * @author debba
 */
public class Event {
  int id;
String nom;
String type;
String date;
String lieu;
String description;
String heure;
int idm;
String img;
String nomImage;

    public Event(int id, String nom, String type, String date, String lieu, String description, String heure, int idm, String img, String nomImage) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.date = date;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.idm = idm;
        this.img = img;
        this.nomImage = nomImage;
    }

    public Event(int id, String nom, String type, String lieu, String description, String heure, int idm, String nomImage) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.idm = idm;
        this.nomImage = nomImage;
    }

    public Event(int id, String nom, String type, String lieu, String description, String heure, int idm) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.idm = idm;
    }

    public Event(String nom, String type, String lieu, String description, String heure, int idm, String nomImage) {
        this.nom = nom;
        this.type = type;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.idm = idm;
        this.nomImage = nomImage;
    }

    public Event() {
    }

    public Event(String nom, String type, String lieu, String description, String heure, int idm) {
        this.nom = nom;
        this.type = type;
        this.lieu = lieu;
        this.description = description;
        this.heure = heure;
        this.idm = idm;
    }





 

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

 

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public int getIdm() {
        return idm;
    }

    public void setIdm(int idm) {
        this.idm = idm;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNomImage() {
        return nomImage;
    }

    public void setNomImage(String nomImage) {
        this.nomImage = nomImage;
    }


}
