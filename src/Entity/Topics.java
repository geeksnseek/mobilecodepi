/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author jalel
 */
public class Topics {

    private int topic_id;
    private String topic_title;

    private String topic_subject;
    private String topic_date;
    private int topic_cat;
    private int topic_by;
    private int etat;
    private String photo;
    private int dispo ;
    

    public Topics() {
    }

    public Topics(int topic_id, String title, String topic_subject, String topic_date, int topic_cat, int topic_by) {
        this.topic_id = topic_id;
        this.topic_title = title;

        this.topic_subject = topic_subject;
        this.topic_date = topic_date;
        this.topic_cat = topic_cat;
        this.topic_by = topic_by;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    

    public int getTopic_id() {
        return topic_id;
    }

    public String getTopic_subject() {
        return topic_subject;
    }

    public String getTitle() {
        return topic_title;
    }

    public String getTopic_date() {
        return topic_date;
    }

    public int getTopic_cat() {
        return topic_cat;
    }

    public int getTopic_by() {
        return topic_by;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public void setTopic_subject(String topic_subject) {
        this.topic_subject = topic_subject;
    }
     public void setTopic_title(String t) {
        this.topic_title = t;
    }

    public void setTopic_date(String topic_date) {
        this.topic_date = topic_date;
    }

    public void setTopic_cat(int topic_cat) {
        this.topic_cat = topic_cat;
    }

    public void setTopic_by(int topic_by) {
        this.topic_by = topic_by;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getDispo() {
        return dispo;
    }

    public void setDispo(int dispo) {
        this.dispo = dispo;
    }
    

    @Override
    public String toString() {
        return "topics{" + "topic_title=" + topic_title + ", topic_date=" + topic_date + ", topic_cat=" + topic_cat + ", topic_by=" + topic_by + '}';
    }

    

 

}
