/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author debba
 */
public class Participant {
 int idp,idevent,idm;   
 String avis,etat;

    public Participant(int idp, int idevent, int idm, String avis, String etat) {
        this.idp = idp;
        this.idevent = idevent;
        this.idm = idm;
        this.avis = avis;
        this.etat = etat;
    }

    public Participant(int idevent, int idm, String avis, String etat) {
        this.idevent = idevent;
        this.idm = idm;
        this.avis = avis;
        this.etat = etat;
    }

    public Participant() {
    }

    public int getIdp() {
        return idp;
    }

    public void setIdp(int idp) {
        this.idp = idp;
    }

    public int getIdevent() {
        return idevent;
    }

    public void setIdevent(int idevent) {
        this.idevent = idevent;
    }

    public int getIdm() {
        return idm;
    }

    public void setIdm(int idm) {
        this.idm = idm;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
 
}
