/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Event;
import static GuiForm.SignInForm.res;
import static GuiForm.SignUpForm.pickerToString;
import Services.EventServices;
import Services.MembreService;

import static com.codename1.charts.util.ColorUtil.CYAN;
import com.codename1.components.ScaleImageLabel;
import com.codename1.io.FileSystemStorage;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;
import rest.file.uploader.tn.FileUploader;

/**
 *
 * @author debba
 */
public class addEventForm extends BaseForm {

    public static TextField nom, type, lieu, description, heure, nomImage, picker;
    public static Picker dPicker;
    Form f;
    String path;
    Button btnajout, imgBtn;
    public static ComboBox box;
    EventServices auth = new EventServices();

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public addEventForm(Resources res) {
        super("Newsfeed", BoxLayout.y());

        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Evenement");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
        });

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 5) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 5);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        Label facebook = new Label("", res.getImage("facebook-logo.png"), "BottomPad");
        Label twitter = new Label("", res.getImage("twitter-logo.png"), "BottomPad");
        facebook.setTextPosition(BOTTOM);
        twitter.setTextPosition(BOTTOM);

        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                        GridLayout.encloseIn(3,
                                facebook,
                                FlowLayout.encloseCenter(
                                        new Label(res.getImage(""), "PictureWhiteBackgrond")),
                                twitter
                        )
                )
        ));

        Validator v = new Validator();
        Validator v2 = new Validator();
        Validator v3 = new Validator();
        f = new Form("ajouter event");
        nomImage = new TextField();
        nom = new TextField("", "Nom", 20, TextField.ANY);
        nom.getAllStyles().setFgColor(0x000000);
        v.addConstraint(nom, new LengthConstraint(4));
        String testusername = "^\\(?([a-z]{3})\\)?";
        type = new TextField("", "Type", 20, TextField.EMAILADDR);
        type.getAllStyles().setFgColor(0x000000);
        v.addConstraint(type, new LengthConstraint(4));
        lieu = new TextField("", "Lieu", 20, TextField.ANY);
        lieu.getAllStyles().setFgColor(0x000000);
        v.addConstraint(lieu, new LengthConstraint(4));
        description = new TextField("", "Description", 20, TextField.ANY);
        description.getAllStyles().setFgColor(0x000000);
        v.addConstraint(description, new LengthConstraint(4));
        box = new ComboBox();
        heure = new TextField("", "Heure", 20, TextField.ANY);
        heure.getAllStyles().setFgColor(0x000000);
        picker = new TextField();
        dPicker = new Picker();
        dPicker.getAllStyles().setFgColor(0x000000);


        btnajout = new Button("ajouter");

        ///////////test charger combobox///////
        Vector<String> vec = new Vector<String>();
        String café = "café", restaurant = "restaurant";
        vec.addElement(café);
        vec.addElement(restaurant);
        box.setModel(new DefaultListModel(vec));
        ////////////////////////////////////////
        imgBtn = new Button("parcourir image");
        try {
            imgBtn.setIcon(Image.createImage("/camera1.png").scaled(30, 30));

        } catch (IOException ex) {
        }
        imgBtn.addActionListener(e -> {
            Display.getInstance().openGallery(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    if (ev != null && ev.getSource() != null) {
                        path = (String) ev.getSource();
                        System.out.println(path.substring(7));
                        Image img = null;
                        nomImage.setText(path.substring(7));//image heya just label nsob feha fel path
                        System.out.println(nomImage);
                        try {
                            img = Image.createImage(FileSystemStorage.getInstance().openInputStream(path));
                            System.out.println(img);

                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }, Display.GALLERY_IMAGE);
        });
/////////////////////////////////////////////////////
        try {
            btnajout.setIcon(Image.createImage("/mark.png").scaled(30, 30));

        } catch (IOException ex) {
        }
       
        addStringValue("", nom);
   
        addStringValue("", type);
      
        addStringValue("", lieu);
     
        addStringValue("", description);
        addStringValue("", heure);
        addStringValue("", dPicker);



        addStringValue("", imgBtn);
        addStringValue("", btnajout);


        btnajout.addActionListener((e) -> {
                       
                Event a = new Event();
                EventServices ser = new EventServices();
                FileUploader fc = new FileUploader("localhost/SymfonyPi/web/imagesEvent");

                Date dat = new Date();
            System.out.println("dPicker.getDate()"+dPicker.getDate());
            System.out.println("dat.getTime()"+dat.getTime());
                int d = (int)(dPicker.getDate().getTime()-dat.getTime());
                System.out.println("hethiiiiii"+d);

        if (d<=0) {
            Dialog.show("erreur", "veuillez entrer une date valide superieure ou égale a la date actuelle", "Ok", null);
        }
        if (nomImage.getText().equals("")) {
            Dialog.show("erreur", "veuillez entrer une image", "Ok", null);
              
        }
       
if(d>0){
try {
    System.out.println("owel el try");
    String f = fc.upload(nomImage.getText());
   
    System.out.println(f);
    System.out.println("ba3d el upload");
   
    if(!nom.getText().isEmpty()&&!type.getText().isEmpty()&&!lieu.getText().isEmpty()&&!description.getText().isEmpty()&&!heure.getText().isEmpty()){
        a = new Event(nom.getText(), type.getText(), lieu.getText(), description.getText(), heure.getText(), MembreService.user.getId(),f);

System.out.println(a);

ser.ajoutEvent(a);
    }
    else{
     Dialog.show("erreur", "Champ vide!", "Ok", null);
    }         

} catch (Exception ex) {
    
}
}
        });

        dPicker.addActionListener(ev -> {
            picker.setText(pickerToString(dPicker));
            System.out.println(picker.getText());
        });

    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
