/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Topics;
import Services.TopicsService;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.androidCode.MyApplication;
import static com.mycompany.androidCode.MyApplication.theme;

/**
 *
 * @author nawre
 */
public class DetailsTopicForm {

    private Button btn, btn2;
    Font fnt = Font.createSystemFont(Font.FACE_MONOSPACE, 0xB7CA79, Font.SIZE_SMALL);
    int size = Display.getInstance().convertToPixels(4);
    Form hi = new Form("Consulter Details", new BoxLayout(BoxLayout.Y_AXIS));

    public DetailsTopicForm(Topics t) {

        hi.setScrollableY(true);

        Container pubInfo = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container con = new Container(new BoxLayout(BoxLayout.X_AXIS));
        pubInfo.setScrollableY(true);
        hi.getToolbar().addCommandToLeftBar("Retour", MyApplication.theme.getImage("back-command.png"), (event) -> {
            TopicsForm mp = new TopicsForm();
            mp.getTopic().show();
        });
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        /*
        if (MyApplication.CurrentUser != null) {
            l.setText("Salut M. " + MyApplication.CurrentUser.getUsername());
        }
        l.getStyle().setFgColor(0xB95656);
        //  l.getStyle().setAlignment(1000);
         */
        Label contenu = new Label();
        contenu.setText(t.getTitle());

        Label sujet = new Label(t.getTopic_subject());
        Label user = new Label("Id user : " + t.getTopic_by());
        /*
        btn = new Button("Modifier", FontImage.createFixed("", fnt, 0xffffff, size, size));
        btn = setButton(btn, 0x1AA09F, 0xffffff);
        /*btn.addActionListener(new ActionListener() {
            });*/
        btn = new Button("modifier");

        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ModifierTopicForm mp = new ModifierTopicForm(t);
                mp.getF().show();
            }
        });

        btn2 = new Button("supprimer");
        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                boolean bool = Dialog.show("Supprimer", "Voulez-vous vraiment supprimer Topic ? ", "Ok", "Cancel");
                if (bool) {
                    TopicsService os = new TopicsService();
                 
                    System.out.println(t.getTopic_id());
                    os.SupprimerOffre(t.getTopic_id());
                    TopicsForm mp = new TopicsForm();
                    mp.getTopic().show();
                    Dialog.show("Suppression", "Suppression affectuée avec succès", "Ok", null);

                } else {
                    DetailsTopicForm mp = new DetailsTopicForm(t);
                    mp.getF().show();
                }
            }

        });

        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(650, 500, 0xffffffff), true);
        String url = "http://localhost/SymfonyPi/web/imagesForum/" + t.getPhoto();
        URLImage urlimage = URLImage.createToStorage(enc, url, url);
        ImageViewer iv = new ImageViewer(urlimage);

        iv.setImage(urlimage);
        //  contenu.add(BorderLayout.NORTH, .scaled(Display.getInstance().getDisplayWidth(), 800));
        //b.setSize(new Dimension(Display.getInstance().getDisplayWidth(), 200));

        pubInfo.add(iv);
        pubInfo.add(sujet);
        pubInfo.add(contenu);
        pubInfo.add(user);

        hi.add(pubInfo);
        //System.out.println(act.getIduser().getId());
        hi.add(btn);
        hi.add(btn2);

        hi.show();

    }

    public Form getF() {
        return hi;
    }

    public void setF(Form f) {
        this.hi = f;
    }

    private Label createForFont1(Font fnt, String s) {
        Label l = new Label(s);
        l.getUnselectedStyle().setFont(fnt);
        return l;
    }

}
