/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Commande;
import Entity.Panier;
import Services.PanierService;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.Date;

/**
 *
 * @author brini
 */
public class CommandeForm extends BaseForm
{
    PanierService service = new PanierService();
    
    public CommandeForm(Resources res)
    {
         super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle(" Mes Commandes ");
        getContentPane().setScrollVisible(false);
     
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
        
        
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() /4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

     
        
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3, 
                            
                            FlowLayout.encloseCenter(
                                new Label(res.getImage(""), "PictureWhiteBackgrond"))
                          
                    )
                )
        ));

        
        Container C1 =new Container(new GridLayout(1,4));
        
        Label   dt = new Label("Date Cmd ");
        Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        dt.getUnselectedStyle().setFont(ttfFont);
        dt.getAllStyles().setFgColor(0x228B22);
        Label   dte = new Label("Date d'éxp ");
       dte.getUnselectedStyle().setFont(ttfFont);
       dte.getAllStyles().setFgColor(0x228B22);
        Label  tot = new Label("Total(Dt)");
       tot.getUnselectedStyle().setFont(ttfFont);
        tot.getAllStyles().setFgColor(0x228B22);
        
        Label pay = new Label("Etat ");
        pay.getUnselectedStyle().setFont(ttfFont);
       pay.getAllStyles().setFgColor(0x228B22);
       
  
        C1.add(dt);
        C1.add(dte);
        C1.add(tot);
        C1.add(pay);
        C1.getStyle().setPaddingBottom(100);
        Container C2 =new Container(BoxLayout.y());
        
       SpanLabel lmsg = new SpanLabel("Votre commande est réservée pendant 10 jours. Au-delà de ce délai, sans réception de paiement votre commande sera annulée.");
        Font tfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        lmsg.	getTextUnselectedStyle().setFont(tfFont);
        lmsg.	getTextAllStyles().setFgColor(0xFF0000);
        lmsg.	getTextAllStyles().setPaddingBottom(100);
        
        C2.add(lmsg);
        C2.add(C1);
        
        
        
        
        
        
       for (Commande c : service.mesCom()) 
        
       {
          C2.add(additem(c, res));
       }
         addStringValue("", C2);
         
         Button btn = new Button("Consultez Vos points ");
         addStringValue("", btn) ;
    }
    
    
     private void addStringValue(String s, Component v) 
     {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    } 
     
       public Container additem(Commande l,Resources res)
    {
       
       Container parent =new Container(new GridLayout(2, 4));
       Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
        String strd = "1"+l.getDate().substring(114,123)+"000";
        
        long timestamp = Long.parseLong(strd);
        Date d = new Date(timestamp );
        SpanLabel ldate = new SpanLabel(d.toString());
        String strd1 = ldate.getText().substring(4, 10);
        String strd2 = ldate.getText().substring(23, 28);
        Label ldaate = new Label(strd1+strd2); 
   
       ldate.getAllStyles().setFgColor(0x0000);
       ldate.getUnselectedStyle().setFont(ttfFont);
       

       
  
       String strd3 = "1"+l.getDateexp().substring(114,123)+"000";
 
       
       
        long timestamp2 = Long.parseLong(strd3);
        Date d1 = new Date(timestamp2 );
       SpanLabel lexp = new SpanLabel(d1.toString());
       String strd4 = lexp.getText().substring(4, 10);
       String strd5 = lexp.getText().substring(23, 28);
       Label ldaatexp = new Label(strd4+strd5); 
       lexp.getAllStyles().setFgColor(0x0000);
       lexp.getUnselectedStyle().setFont(ttfFont);
       Label  ltot = new Label("   " + l.getTotal() + "");
       ltot.getUnselectedStyle().setFont(ttfFont);
       ltot.getAllStyles().setFgColor(0x0000);
       Label lpay ;
       if (l.getPayement().equals("non paye"))
           
       {
           lpay = new Label();
           lpay.setText("non payé");
           lpay.getAllStyles().setFgColor(0x0000);
           lpay.getUnselectedStyle().setFont(ttfFont);
           
       }
       else
       {
           lpay = new Label();
           lpay.setText("payé");
           lpay.getAllStyles().setFgColor(0xFF0000);
           lpay.getUnselectedStyle().setFont(ttfFont);
       }
      
       parent.add(ldaate).add(ldaatexp).add(ltot).add(lpay);
       
       return parent ;
      
        
        
    }
    
    
}
