/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Categorie;
import Entity.Topics;
import Services.CategorieService;
import Services.TopicsService;
import com.codename1.capture.Capture;
import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextComponent;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author nawre
 */
public class AjouterTopicForm {

    private Form ajouttopic;
    private Container cntl;
    private Label titre1, sujet;
    private TextField subj, titre;
    private Button add, addimg;
    private String filePath;
    private ComboBox<String> cb;
    ImageViewer img;
    
    public AjouterTopicForm() {
        Font fnt = Font.createSystemFont(Font.FACE_MONOSPACE, 0xB7CA79, Font.SIZE_SMALL);
        int size = Display.getInstance().convertToPixels(4);
        FontImage fm = FontImage.createFixed("\uecc6", fnt, 0xffffff, size, size);
        Font largeBoldSystemFont = Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE);
        ajouttopic = new Form("Ajouter Topic", new FlowLayout(Container.CENTER, Container.CENTER));
        ajouttopic.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_ARROW_BACK, (evt) -> {
            TopicsForm a = new TopicsForm();
            a.getTopic().show();
        });
        try {
            img = new ImageViewer(Image.createImage("/load.png"));
        } catch (IOException ex) {
            
        }
        cb = new ComboBox();
        
        CategorieService es = new CategorieService();
        List<Categorie> le = es.getAllCat();
        for (Categorie e : le) {
            cb.addItem(e.getCat_name());
            
        }
        titre = new TextField("", "Title");
        subj = new TextField("", "Sujet", 20, 0);
        addimg = new Button("Ajouter une Photo");
        addimg.addActionListener((evt) -> {
            filePath = Capture.capturePhoto(Display.getInstance().getDisplayWidth(), -1);
            try {
                img = new ImageViewer(Image.createImage(filePath));
                
                System.out.println(filePath);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            ajouttopic.refreshTheme();
        });
        cntl = new Container(BoxLayout.y());
        
        add = new Button("Ajouter");
        
        add.addActionListener((evt) -> {
            TopicsService ts = new TopicsService();
           // CategorieService cs = new CategorieService();
            Topics o = new Topics();
            if ((titre.getText() != "") && (subj.getText() != "")) {
                // o.setPhoto(UploadImg.Upload(filePath));

                o.setTopic_title(titre.getText());
                o.setTopic_subject(subj.getText());
                String str = cb.getSelectedItem();
                o.setTopic_cat(ts.getCatID(str));
                
                ts.ajouterTopic(o);
                Dialog.show("Ajout", "Topic ajouté", "Ok", null);
                TopicsForm tf = new TopicsForm();
                tf.getTopic().showBack();
            } else {
                Dialog.show("Alert", "Veuillez remplir tout les champs ", "Ok", null);
            }
        });
        cntl.add(titre);
        cntl.add(subj);
        cntl.add(cb);
        cntl.add(add);
        
        ajouttopic.add(cntl);
        
    }
    
    public Form getAjouttopic() {
        return ajouttopic;
    }
    
}
