/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import com.codename1.components.FloatingHint;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.ComponentGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;

/**
 *
 * @author brini
 */
public class MailStockForm extends BaseForm
{
    public MailStockForm(Resources res)
    { super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle(" Envoyer Mail au  SMART CYCLE ");
        getContentPane().setScrollVisible(false);
     
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
        
        
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() /4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

     
        
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3, 
                            
                            FlowLayout.encloseCenter(
                                new Label(res.getImage(""), "PictureWhiteBackgrond"))
                          
                    )
                )
        ));

        addStringValue("", createDemo(res));
    }
           
    
    
    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    } 
     public Container createDemo(Resources res)
    {
        Container message = new Container(new BoxLayout(BoxLayout.Y_AXIS));        
        ComponentGroup gp = new ComponentGroup();
        message.addComponent(gp);
        Label lb = new Label("Envoyer à ");
        Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        lb.getUnselectedStyle().setFont(ttfFont);
        lb.getAllStyles().setFgColor(0x228B22);
        final TextField to = new TextField("", "Mail", 20, TextField.ANY);
        to.getAllStyles().setFgColor(0x000000);
        to.setSingleLineTextArea(true);
//        to.setHint("briniskhawla2@gmail.com");
          gp.addComponent(lb);
        gp.addComponent(to);
        Label l = new Label("Sujet ");
        l.getUnselectedStyle().setFont(ttfFont);
          l.getAllStyles().setFgColor(0x228B22);
        final TextField subject = new TextField("", "Sujet", 20, TextField.ANY);
         subject.setSingleLineTextArea(true);
         subject.getAllStyles().setFgColor(0x000000);
        gp.addComponent(l);
        gp.addComponent(subject);
        final TextField body = new TextField("", "Try it out at http://www.codenameone.com/", 20, TextField.ANY);
        body.setSingleLineTextArea(true);
        body.setRows(4);
        body.getAllStyles().setFgColor(0x000000);
        body.setHint("Message Body");
        gp.addComponent(body);
        
        ComponentGroup buttonGroup = new ComponentGroup();
        Button btn = new Button("Send");
        buttonGroup.addComponent(btn);
        message.addComponent(buttonGroup);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                Message msg = new Message(body.getText());
                Display.getInstance().sendMessage(new String[] {to.getText()}, subject.getText(), msg);
                        Dialog ip = new InfiniteProgress().showInifiniteBlocking();
                         ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("Votre mail a été bien envoyé ."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                new Stock2Form(res);
            }
        });
                
        return message;
    }
     
}
