/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Event;
import static GuiForm.CodeCadeauEventForm.code;
import Services.EventServices;
import com.codename1.components.ImageViewer;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;

/**
 *
 * @author debba
 */
public class ShowMyParticipationForm extends BaseForm {

    public static Form f, form;
    public static SpanLabel lb;
    public static Label TitleShow;
    public static String url = "http://localhost/SymfonyPi/web/imagesEvent";
    public static Button suppbtn, modifbtn, btnchercher, partagerbtn, archivebtn, participerbtn, Avisbtn, Cadeaubtn;
    public static TextField titrecherche;
    public static Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);

    public ShowMyParticipationForm(Resources res) {
        // theme = UIManager.initFirstTheme("/theme_1");
//       

        super("Newsfeed", BoxLayout.y());
//        UIBuilder ui = new UIBuilder(); 
//        Container ct1 = ui.createContainer(res, "Filtrage");
//         Form f= (Form)ct1;
        //f = new Form(BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Evenement");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
        });

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 5) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 5);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        Label facebook = new Label("", res.getImage("facebook-logo.png"), "BottomPad");
        Label twitter = new Label("", res.getImage("twitter-logo.png"), "BottomPad");
        facebook.setTextPosition(BOTTOM);
        twitter.setTextPosition(BOTTOM);

        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                        GridLayout.encloseIn(3,
                                facebook,
                                FlowLayout.encloseCenter(
                                        new Label(res.getImage(""), "PictureWhiteBackgrond")),
                                twitter
                        )
                )
        ));

        lb = new SpanLabel("");
        archivebtn = new Button("Archive des Evenements");

//        archivebtn.addActionListener((e) -> {
//            Archive a = new Archive(res);
//            a.getF().show();
//        });
//        add(archivebtn);
        titrecherche = new TextField();
        titrecherche.setHint("Tapez votre recherche ici");
        titrecherche.getAllStyles().setFgColor(0xE30304);

        btnchercher = new Button("chercher");
        Cadeaubtn = new Button("Consulter mes points cadeau");
        add(titrecherche);
        add(btnchercher);
        add(Cadeaubtn);
        TitleShow = new Label();
        TitleShow.getAllStyles().setFgColor(0xE30304);
        TitleShow.getUnselectedStyle().setFont(ttfFont);
        TitleShow.getStyle().setPaddingLeft(400);
        TitleShow.getStyle().setPaddingBottom(100);
        TitleShow.setText("Ma participation");
        add(TitleShow);
        EventServices SP = new EventServices();
        btnchercher.addActionListener((e) -> {
            Form F2 = new Form(BoxLayout.y());
          
            String d = titrecherche.getText();
            ArrayList<Event> liche = SP.ChercherEvent(d);
            for (Event lis : liche) {
                System.out.println("bdinaaaaaa");
                ImageViewer imgV = new ImageViewer();
                Image placeholder = Image.createImage(600, 600);
                EncodedImage enc = EncodedImage.createFromImage(placeholder, false);
                URLImage urlim = URLImage.createToStorage(enc, url + "/" + lis.getNomImage(), url + "/" + lis.getNomImage());
                imgV.setImage(urlim);
                System.out.println();
                System.out.println(url + "/" + lis.getNomImage());
                Label aa = new Label("Nom  : " + lis.getNom());
                Label type = new Label("Type  : " + lis.getType());
                Label detailsEv = new Label("Voir Details ");
                detailsEv.getAllStyles().setTextDecoration(Style.TEXT_DECORATION_UNDERLINE);
                Container jjj = new Container(BoxLayout.y());
                Container kkk = new Container(BoxLayout.x());
                jjj.addAll(aa, type, detailsEv);
                kkk.addAll(imgV, jjj);
                F2.add(kkk);

                F2.getToolbar().addCommandToLeftBar("back", null, (j) -> {
//                    Affichage h = new Affichage(res);
//                    h.getF().show();

                });
                F2.getToolbar().addCommandToLeftBar("", res.getImage("retourner.png"), eve -> {
                    ShowAllEventsForm h = new ShowAllEventsForm(res);
                    h.show();
                });
                F2.show();

            }

        });
        Cadeaubtn.addActionListener((eeee) -> {
//                        EventServices ser = new EventServices();
//                        SP.supprimerEvent(li, res);
System.out.println("nbbbbbb"+code);
            ArrayList<Float> nbparticip = SP.ChercherNombre();
            Float nbpar = nbparticip.get(0);
            if (nbpar <= 2) {
                Dialog.show("Echec", "Vous n'êtes pas assez actif! Participez de plus avec nous", "ok", null);
            }

            if ((nbpar > 2) && (nbpar <= 4)) {
                Dialog.show("Echec", "Vous êtes proche de ganger avec nous! Participez davantage", "ok", null);
            }
            if (nbpar >= 5) {
                Dialog.show("Succès", "Bravo! Vous êtes ami de la nature, Cliquez ok et entrez le code pour obtenir ton cadeau", "ok", null);
                CodeCadeauEventForm h = new CodeCadeauEventForm(res);
                h.getF().show();
            }
        });

        // f.add(lb);
        ArrayList<Event> lis = SP.getListMyParticipation();
        f = new Form(BoxLayout.y());

        for (Event li : lis) {
            System.out.println("owel el for");
            Label aa1 = new Label("Nom  : " + li.getNom());
            //Label date = new Label("Date   :  " +SP.getDateS());
            Label aa2 = new Label("Type  : " + li.getType());
            Label aa3 = new Label("Lieu  : " + li.getLieu());
            Label desc = new Label("Description : " + li.getDescription());
            Label heure = new Label("Heure : " + li.getHeure());
            Label detailsEv = new Label("Voir Details ");
            detailsEv.getAllStyles().setTextDecoration(Style.TEXT_DECORATION_UNDERLINE);
            Container cc = new Container(BoxLayout.x());
            Container c = new Container(BoxLayout.y());
            Container k = new Container(BoxLayout.x());
            suppbtn = new Button("Supprimer");
            modifbtn = new Button("Modifier");
            partagerbtn = new Button("Partager");
            participerbtn = new Button("Annuler");
            Avisbtn = new Button("Donner Avis");

            Image placeholder = Image.createImage(600, 600);
            EncodedImage enc = EncodedImage.createFromImage(placeholder, false);
            URLImage urlim = URLImage.createToStorage(enc, li.getNomImage(), url + "/" + li.getNomImage());
            System.out.println(url + "/" + li.getNomImage());
            System.out.println(urlim);
            ImageViewer imgV = new ImageViewer();
            imgV.setImage(urlim);
            // TextField a = new TextField(li.getId());

            //Label d = new Label(li.getDate().toString());
            //c.add(a);
            c.add(aa1);
            //c.add(date);
            c.add(aa2);
            c.add(detailsEv);
//            c.add(aa3);
//            c.add(desc);
//            c.add(heure);
            //c.add(d);
            //cc.add(imgV);
            //cc.add(c);
            //c.add(imgV);
            c.getStyle().setPaddingLeft(200);
            c.getStyle().setPaddingTop(200);
            k.addAll(imgV, c);
            k.setSameWidth();
            k.setSameHeight();
            //f.add(c);
            System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            add(k);
            //f.show();
            detailsEv.addPointerPressedListener((l) -> {

                //     AEvent a = new AEvent();
                form = new Form(BoxLayout.y());
                Label lbser = new Label();
                lbser.getAllStyles().setFgColor(0x228B22);

                lbser.getUnselectedStyle().setFont(ttfFont);
                Label lbser1 = new Label();
                lbser1.getUnselectedStyle().setFont(ttfFont);
                Label lbser2 = new Label();
                lbser2.getUnselectedStyle().setFont(ttfFont);
                Label lbser3 = new Label();
                lbser3.getUnselectedStyle().setFont(ttfFont);
                Label lbser4 = new Label();
                lbser4.getUnselectedStyle().setFont(ttfFont);
                Label Title = new Label("Les details sur l'évenement");
                Title.getAllStyles().setFgColor(0xE30304);
                Title.getUnselectedStyle().setFont(ttfFont);
                Title.getStyle().setPaddingLeft(300);
                Title.getStyle().setPaddingBottom(100);

                Container F3 = new Container(BoxLayout.y());
                F3.add(Title);

                F3.add(lbser);
                F3.add(lbser1);
                F3.add(lbser2);
                F3.add(lbser3);
                F3.add(lbser4);

                System.out.println("imaage");

                EncodedImage en = EncodedImage.createFromImage(placeholder, false);
                URLImage urli = URLImage.createToStorage(en, url + "/" + li.getNomImage(), url + "/" + li.getNomImage());
                ImageViewer img1 = new ImageViewer();
                img1.setImage(urli);

                F3.add(img1);

                ConnectionRequest con = new ConnectionRequest();
                ConnectionRequest con2 = new ConnectionRequest();

                String url = "http://localhost/SymfonyPi/web/app_dev.php/mobile/findEvent/" + li.getId();
                con.setUrl(url);

                con.addResponseListener((le) -> {
//          form.getToolbar().addCommandToLeftBar("back", null, (ev)-> new ShowMyEventsForm().ShowMyEventsForm(res).show());
//         form.getToolbar().addCommandToLeftBar("back", null, (ev)->{
//      //  ShowMyEventsForm h=new ShowMyEventsForm(res);
//          f.show(); 
//        };
                    String reponse = new String(con.getResponseData());
                    System.out.println(reponse);
                    lbser.setText("Nom   :   " + li.getNom());
                    lbser1.setText("Description  :  " + li.getDescription());
                    lbser2.setText("Lieu  :   " + li.getLieu());
                    lbser3.setText("Heure  :   " + li.getHeure());
                    lbser4.setText("Type  :   " + li.getType());
                    suppbtn.addActionListener((eeee) -> {
                        EventServices ser = new EventServices();
                        SP.supprimerEvent(li, res);

                    });
                    modifbtn.addActionListener((ej) -> {
                        UpdateEventForm h = new UpdateEventForm(li, res);
                        h.getF().show();

                    });
                    participerbtn.addActionListener((ejjj) -> {
                        SP.participerEvent(li);

//                     UpdateEventForm h = new UpdateEventForm(li, res);
//                       h.getF().show();
                    });
                    Avisbtn.addActionListener((ejjj) -> {
                        AvisEventForm h = new AvisEventForm(li, res);
                        h.getF().show();
//                     UpdateEventForm h = new UpdateEventForm(li, res);
//                       h.getF().show();
                    });
                    partagerbtn.addActionListener((e) -> {
                        //                    ServiceEvent ser = new ServiceEvent();
////                      String filePath = (String) li.getImage();
////                       int fileNameIndex = filePath.lastIndexOf("/") + 1;
////                     String fileName = filePath.substring(fileNameIndex);
////                     try {
////                         ser.shareImageOnFacebook(filePath,li.getTitre(),li.getDescription());
////                     } catch (IOException ex) {
////                     }
//
                    });

                    //                  F3.add(suppbtn);
                    //                   F3.add(modifbtn);
                    F3.add(partagerbtn);
                    F3.add(participerbtn);
                    F3.add(Avisbtn);

                    // HomeForm ft = new HomeForm();
                    // ft.getF().show();
                });

                NetworkManager.getInstance().addToQueueAndWait(con);

                System.out.println("test el F3");
//                add(F3);
                // f.add(F3);
                form.add(F3);
                form.getToolbar().addCommandToLeftBar("", res.getImage("retourner.png"), eve -> {
                    new ShowMyParticipationForm(res).show();
                    //  ShowAllEventsForm h = new ShowAllEventsForm(res);
                    //    h.show();
                });
//                                  form.getToolbar().addCommandToLeftBar("back", null, (ev)->{
//        ShowMyEventsForm h=new ShowMyEventsForm(res);
//          h.getF().show(); 
//        });
                form.show();
                //  f.add(lbser);

            });
            //    k.setLeadComponent(aa1);

            //           
            System.out.println("test ba3d a");
//             F3.show();

        }
        lb.setText(lis.toString());

//                  f.getToolbar().addCommandToLeftBar("back", null, (ev)->{
//        ShowMyEventsForm h=new ShowMyEventsForm(res);
//          h.getF().show(); 
//        });
        //f.show();
//        return f;
        //f.show();
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
