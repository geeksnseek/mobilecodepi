/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Panier;
import Entity.Stock;
import Services.PanierService;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.io.IOException;

/**
 *
 * @author brini
 */
public class PanierForm extends BaseForm
{
    Form f ;
    PanierService service = new PanierService();
    public PanierForm(Resources res)
    {

        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle(" Votre Panier ");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {});


        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() /3);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);



        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3,

                            FlowLayout.encloseCenter(
                                new Label(res.getImage(""), "PictureWhiteBackgrond"))

                    )
                )
        ));


        Container C1 =new Container(new GridLayout(1, 6));

        Label   Prix = new Label("   Prix ");
        Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
        Prix.getUnselectedStyle().setFont(ttfFont);
        Prix.getAllStyles().setFgColor(0x228B22);
        Label   Total = new Label("  Total ");
        Total.getUnselectedStyle().setFont(ttfFont);
        Total.getAllStyles().setFgColor(0x228B22);
        Label   desc = new Label("Produits  ");
        desc.getUnselectedStyle().setFont(ttfFont);
        desc.getAllStyles().setFgColor(0x228B22);

        Label  Quantite = new Label("  Qte ");
        Quantite.getUnselectedStyle().setFont(ttfFont);
        Quantite.getAllStyles().setFgColor(0x228B22);


        C1.add(desc);
        C1.add(Quantite);
        C1.add(Prix);
        C1.add(Total);
        C1.getStyle().setPaddingBottom(100);
        Container C2 =new Container(BoxLayout.y());
        C2.add(C1);

        Button valider = new Button(" Valider ");
        Button retour= new Button("Continuer mes achats");
        Button supp = new Button("Supprimer ma commande");

         if (service.affichepanier().size()==0)
       {

        Label lb = new Label("Votre panier est vide ");
          lb.getStyle().setPaddingLeft(300);
        Font tfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);
        lb.getUnselectedStyle().setFont(ttfFont);
        lb.getAllStyles().setFgColor(0x228B22);
//          addStringValue("", C1);
            addStringValue("", lb);
//            addStringValue("", retour);
       }
       else

         {
       for (Panier p : service.affichepanier())
       {
              System.out.println( p.getIdStock().getDescription());
           C2.add(additem(p,res));


       }



       retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               Stock2Form stock = new Stock2Form(res);
               stock.show();
            }
        });

       supp.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt)
           {
       Dialog ip = new InfiniteProgress().showInifiniteBlocking();
                     service.suppCommande();
                        ip.dispose();
                            new PanierForm(res).show();


           }
       });

       valider.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt)
           {
               service.validerCom();
              // new PaiementForm(res).show();

           }
       });





        Container C3 = new Container(BoxLayout.y());
        C3.add(valider);

        Container C4 = new Container(BoxLayout.x());
        C4.add(retour);
        C4.getStyle().setPaddingRight(0);
        C4.add(C3);

        Container C5 = new Container(BoxLayout.x());
        C5.add(supp);


            addStringValue("", C2);
            addStringValue("", C4);
            addStringValue("", C5);

         }



    }
  private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }

    public Container additem(Panier l,Resources res)
    {

       Container parent =new Container(new GridLayout(4, 6));
       Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);

       Label ldes = new Label ("" + l.getIdStock().getDescription() + "");

       ldes.getAllStyles().setFgColor(0x0000);
       ldes.getUnselectedStyle().setFont(ttfFont);

       Label  lQte = new Label("   " + l.getQuantite() + "");
       lQte.getAllStyles().setFgColor(0x0000);
       lQte.getUnselectedStyle().setFont(ttfFont);
       Label  lPrix = new Label("   " + l.getPrix() + "");
       lPrix.getUnselectedStyle().setFont(ttfFont);
       lPrix.getAllStyles().setFgColor(0x0000);
       Label  ltot = new Label("  " + l.getTotal() + "");
       ltot.getAllStyles().setFgColor(0x0000);
       ltot.getUnselectedStyle().setFont(ttfFont);


        Button  modf = new Button("Modifier","Label");
        modf.getUnselectedStyle().setFont(ttfFont);
        modf.getAllStyles().setFgColor(0x228B22);

        Button  supp = new Button("Supprimer","Label");
        supp.getUnselectedStyle().setFont(ttfFont);
        supp.getAllStyles().setFgColor(0x228B22);

       supp.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt) {
                   Dialog ip = new InfiniteProgress().showInifiniteBlocking();
           service.supplc(l.getId());
           Dialog.show("Confirmation", "Suppression effectué avec succés ", "Ok", null);

                    ip.dispose();
                    parent.remove();

           }
       });

       modf.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent evt)
           {
           ModifPanierForm modif = new ModifPanierForm(l,res);
           modif.getF().show();
           }
       });
       parent.add(ldes);
       parent.add(lQte);
       parent.add(lPrix);
       parent.add(ltot);

       parent.add(modf);
       parent.add(supp);

       return parent ;
    }
}
