/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Services.MembreService;
import Services.ProfileClientServices;
import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;


/**
 *
 * @author debba
 */
public class ProfileForm extends BaseForm{

   public static TextField username,password,nom,adresse,email;
   ProfileClientServices pf = new ProfileClientServices();
    

    public ProfileForm(Resources res) {
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Profile");
        getContentPane().setScrollVisible(false);
        
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
        
        
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 3);
            
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        Label facebook = new Label("786 followers", res.getImage("facebook-logo.png"), "BottomPad");
        Label twitter = new Label("486 followers", res.getImage("twitter-logo.png"), "BottomPad");
        facebook.setTextPosition(BOTTOM);
        twitter.setTextPosition(BOTTOM);
        
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3, 
                            facebook,
                            FlowLayout.encloseCenter(
                                new Label(res.getImage("profile-pic.jpg"), "PictureWhiteBackgrond")),
                            twitter
                    )
                )
        ));

       username = new TextField(MembreService.user.getSurnom());
        username.setUIID("TextFieldBlack");
        addStringValue("Username", username);

        password = new TextField(MembreService.user.getMotdepasse(), "Password", 20, TextField.PASSWORD);
        password.setUIID("TextFieldBlack");
        addStringValue("Password", password);

        nom = new TextField(MembreService.user.getNom(),"Nom",20,TextField.ANY);
        nom.setUIID("TextFieldBlack");
        addStringValue("Nom", nom);
        
        adresse = new TextField(MembreService.user.getAdresse(),"Adresse",20,TextField.ANY);
        adresse.setUIID("TextFieldBlack");
        addStringValue("Adresse", adresse);
        
        email = new TextField(MembreService.user.getEmail(), "E-Mail", 20, TextField.EMAILADDR);
        email.setUIID("TextFieldBlack");
        addStringValue("E-Mail", email);
        
        
        
        
        
          Validator val = new Validator();
        
                val.addConstraint(username, new LengthConstraint(4));
                String testusername="^\\(?([a-z]{3})\\)?";
               val.addConstraint(username, new RegexConstraint(testusername, ""));
               
                val.addConstraint(password, new LengthConstraint(4));
               
            
              
                 val.addConstraint(nom, new LengthConstraint(4));
                String testnom="^\\(?([a-z]{3})\\)?";
               val.addConstraint(nom, new RegexConstraint(testnom, ""));
            
                  val.addConstraint(adresse, new LengthConstraint(4));
               String testadresse="^\\(?([a-z]{3})\\)?";
               val.addConstraint(adresse, new RegexConstraint(testadresse, ""));
       
                        
        val.addConstraint(email, RegexConstraint.validEmail());

        
        
        
        
//        
        Button edit = new Button("Modifier");
        addStringValue("", edit);
        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                pf.Modifier();
            }
        });

    
}

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
