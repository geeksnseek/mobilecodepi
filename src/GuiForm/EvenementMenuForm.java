/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import static GuiForm.AssociationMenuForm.image;
import static GuiForm.AssociationMenuForm.tadresse;
import static GuiForm.AssociationMenuForm.tcapital;
import static GuiForm.AssociationMenuForm.tnom;
import Services.ProfileClientServices;
import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Display;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;

/**
 *
 * @author debba
 */
public class EvenementMenuForm extends BaseForm{
       public static TextField tnom,tadresse,tcapital,image;
   Button btnajout,btnaff,btnmanage,btnparti,btnstat;
    String path;
   ProfileClientServices pf = new ProfileClientServices();
    public EvenementMenuForm(Resources res) {
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Espace Événement");
        getContentPane().setScrollVisible(false);
        

        btnajout = new Button("Ajouter mon événement");
        btnaff= new Button("Tous les événements");
        btnmanage= new Button("Gérer mes événements");
        btnparti= new Button("Gérer ma participation");
        btnstat= new Button("Les types les plus utilisés");
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
  
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 5) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 5);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3,FlowLayout.encloseCenter(new Label(res.getImage(""), "PictureWhiteBackgrond"))     )
                )
        ));

                    btnajout.addActionListener((e) -> {
                        new addEventForm(res).show();
                 
        });
      
        btnaff.addActionListener((e)->{
            new ShowAllEventsForm(res).show();  
        });
        
        
        btnmanage.addActionListener(e -> {
           new ShowMyEventsForm(res).show();
        });
    btnparti.addActionListener(e -> {
           new ShowMyParticipationForm(res).show();
        });
    btnstat.addActionListener(e -> {
 ApiStat ap=new ApiStat();
ap.createPieChartForm(res).show();
        });
        
        addStringValue("", btnaff);
        addStringValue("", btnajout);
        addStringValue("", btnmanage);
        addStringValue("", btnparti);
        addStringValue("", btnstat);
        
    
}    


   private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
