///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package GuiForm;
//
//import com.codename1.components.InfiniteProgress;
//import com.codename1.components.InteractionDialog;
//import com.codename1.components.ScaleImageLabel;
//import com.codename1.components.SpanLabel;
//import com.codename1.ui.Button;
//import com.codename1.ui.Component;
//import com.codename1.ui.Dialog;
//import com.codename1.ui.Display;
//import com.codename1.ui.Form;
//import com.codename1.ui.Image;
//import com.codename1.ui.Label;
//import com.codename1.ui.TextField;
//import com.codename1.ui.Toolbar;
//import com.codename1.ui.geom.Dimension;
//import com.codename1.ui.layouts.BorderLayout;
//import com.codename1.ui.layouts.BoxLayout;
//import com.codename1.ui.layouts.FlowLayout;
//import com.codename1.ui.layouts.GridLayout;
//import com.codename1.ui.layouts.LayeredLayout;
//import com.codename1.ui.plaf.Style;
//import com.codename1.ui.util.Resources;
//
//import com.stripe.exception.APIConnectionException;
//import com.stripe.exception.APIException;
//import com.stripe.exception.AuthenticationException;
//import com.stripe.exception.CardException;
//import com.stripe.exception.InvalidRequestException;
//
//
//import Services.ServicePaiement;
//import com.codename1.payment.Purchase;
//import com.codename1.ui.Container;
//import com.codename1.ui.events.ActionEvent;
//import com.codename1.ui.events.ActionListener;
//
///**
// *
// * @author brini
// */
//public class PaiementForm extends BaseForm
//{
//      Form f;
//    TextField carte;
//    TextField mdp;
//    Button conf;
//   public PaiementForm(Resources res)
//   {
//        super("", BoxLayout.y());
//        Toolbar tb = new Toolbar(true);
//        setToolbar(tb);
//        getTitleArea().setUIID("Container");
//        setTitle(" Paiement ");
//        getContentPane().setScrollVisible(false);
//     
//        super.addSideMenu(res);
//        
//        tb.addSearchCommand(e -> {});
//        
//        
//        Image img = res.getImage("profile-background.jpg");
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() /3);
//        }
//        ScaleImageLabel sl = new ScaleImageLabel(img);
//        sl.setUIID("BottomPad");
//        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
//
//     
//        
//        add(LayeredLayout.encloseIn(
//                sl,
//                BorderLayout.south(
//                    GridLayout.encloseIn(3, 
//                            
//                            FlowLayout.encloseCenter(
//                                new Label(res.getImage(""), "PictureWhiteBackgrond"))
//                          
//                    )
//                )
//        ));
//        
//        addStringValue("", createDemo());
//        
////          f = new Form("Panier", BoxLayout.y());
////
////        carte = new TextField();
////        carte.setHint("Num carte");
////        carte.getAllStyles().setFgColor(0);
////        mdp = new TextField();
////        mdp.setHint("Mot de passe");
////        mdp.getAllStyles().setFgColor(0);
////        conf = new Button("Payer");
////        Button retoure = new Button("retour");
////        f.add(carte);
////        f.add(mdp);
////        f.add(conf);
////        f.add(retoure);
////        retoure.addActionListener(e -> {
////            new PanierForm(res).show();
////
////        });
////        conf.addActionListener((e) -> {
////            String carteS = carte.getText();
////            String mdpS = mdp.getText();
////            Dialog ip = new InfiniteProgress().showInifiniteBlocking();
////            if (carteS.equals("") || mdpS.equals(""))
////            {
////                ip.dispose();
////                InteractionDialog dlg = new InteractionDialog("Notification");
////                dlg.setLayout(new BorderLayout());
////                dlg.add(BorderLayout.CENTER, new SpanLabel("Un champ est vide! Veuillez le remplir."));
////                Button close = new Button("Close");
////                close.addActionListener((ee) -> dlg.dispose());
////                dlg.addComponent(BorderLayout.SOUTH, close);
////                Dimension pre = dlg.getContentPane().getPreferredSize();
////                dlg.show(50, 100, 30, 30);
////                return;
////            }
////             int  carteI = 0;
////            try
////            {
////                carteI = Integer.parseInt(carteS);
////            } catch (Exception ex)
////            {
////                ip.dispose();
////                InteractionDialog dlg = new InteractionDialog("Notification");
////                dlg.setLayout(new BorderLayout());
////                dlg.add(BorderLayout.CENTER, new SpanLabel("S'il vous plait, inserez des chiffres dans le champ de carte."));
////                Button close = new Button("Close");
////                close.addActionListener((ee) -> dlg.dispose());
////                dlg.addComponent(BorderLayout.SOUTH, close);
////                Dimension pre = dlg.getContentPane().getPreferredSize();
////                dlg.show(50, 100, 30, 30);
////                return;
////            }
////
////            if (  1 ==1  )
////            {
////                ip.dispose();
////                InteractionDialog dlg = new InteractionDialog("Notification");
////                dlg.setLayout(new BorderLayout());
////                dlg.add(BorderLayout.CENTER, new SpanLabel("Paiement effectué avec succes !."));
////                ServicePaiement spr = new ServicePaiement();
////                try {
////                    try {
////                        spr.payer("4242424242424242", 12, 19, "123", 80, "payment valide");
////                    } catch (InvalidRequestException ex) {
////                    
////                    } catch (APIConnectionException ex) {
////             
////                    } catch (CardException ex) {
////                     
////                    } catch (APIException ex) {
////              
////                    }
////                } catch (AuthenticationException ex) {
////
////                } 
////                Button close = new Button("Close");
////                close.addActionListener((ee) -> {
////                    dlg.dispose();
////                    new PanierForm(res).show();
////                });
////                dlg.addComponent(BorderLayout.SOUTH, close);
////                Dimension pre = dlg.getContentPane().getPreferredSize();
////                dlg.show(50, 100, 30, 30);
////            } 
////            else 
////            {
////                ip.dispose();
////                InteractionDialog dlg = new InteractionDialog("Notification");
////                dlg.setLayout(new BorderLayout());
////                dlg.add(BorderLayout.CENTER, new SpanLabel("les chiffres de carte est incorrecte"));
////                Button close = new Button("Close");
////                close.addActionListener((ee) -> {
////                    dlg.dispose();
////                });
////                dlg.addComponent(BorderLayout.SOUTH, close);
////                Dimension pre = dlg.getContentPane().getPreferredSize();
////                dlg.show(50, 100, 30, 30);
////            }
////
////        });
////
////       addStringValue("", f);
////        
////        
//
//
//
//
//
//   }
//    private static final String[] ITEM_IDS = {
//        "ITEM001",
//        "ITEM002",
//        "ITEM003"
//    };
//    private static final String[] ITEM_NAMES = {
//        "Give Us A Little Money",
//        "Give Us Some More Money",
//        "Thanks!!!"
//    };
//    public String getDisplayName() {
//        return "Purchase";
//    }
//
////    public Image getDemoIcon() {
////        return getResources().getImage("evolution-contacts.png");
////    }
//
//    public Container createDemo()
//    {
//        final Container purchaseDemo = new Container(new BoxLayout(BoxLayout.Y_AXIS));
//        final Purchase p = Purchase.getInAppPurchase();
//        
//        if(p != null) {
//            if(p.isManualPaymentSupported())
//            {
//                purchaseDemo.addComponent(new Label("Manual Payment Mode"));
//                final TextField tf = new TextField("100");
//                tf.getAllStyles().setFgColor(0x228B22);
//                tf.setHint("Send us money, thanks");
//                Button sendMoney = new Button("Send Us Money");
//                sendMoney.addActionListener(new ActionListener() {
//                    public void actionPerformed(ActionEvent evt) {
//                        p.pay(Double.parseDouble(tf.getText()), "USD");
//                    }
//                });
//                purchaseDemo.addComponent(tf);
//                purchaseDemo.addComponent(sendMoney);
//            } 
////            if(p.isManagedPaymentSupported()) {
////                purchaseDemo.addComponent(new Label("Managed Payment Mode"));
////                for(int iter = 0 ; iter < ITEM_NAMES.length ; iter++) {
////                    Button buy = new Button(ITEM_NAMES[iter]);
////                    final String id = ITEM_IDS[iter];
////                    buy.addActionListener(new ActionListener() {
////                        public void actionPerformed(ActionEvent evt) {
////                            p.purchase(id);
////                        }
////                    });
////                    purchaseDemo.addComponent(buy);
////                }
////            } 
//        } else
//        {
//            purchaseDemo.addComponent(new Label("Payment unsupported on this device"));
//        }
//        
//        return purchaseDemo;
//    }
//    
//     private void addStringValue(String s, Component v) {
//        add(BorderLayout.west(new Label(s, "PaddedLabel")).
//                add(BorderLayout.CENTER, v));
//        add(createLineSeparator(0xeeeeee));
//    } 
//
//}
