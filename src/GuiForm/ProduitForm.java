/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package GuiForm;

import Entity.Produit;
import Services.ProduitService;
import com.Pidev.com.Session;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;

/**
 * Represents a user profile in the app, the first form we open after the
 * walkthru
 *
 * @author Shai Almog
 */
public class ProduitForm extends SideMenuBaseForm {

    public ProduitForm(Resources res) {
        super(BoxLayout.y());
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());

        Container titleCmp = BoxLayout.encloseY(
                FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                new Label("Produit", "Title"),
                                new Label("List de Produit", "SubTitle")
                        )
                ).add(BorderLayout.WEST, profilePicLabel)
        );
        tb.setTitleComponent(titleCmp);

        ProduitService servp = new ProduitService();

        for (Produit p : servp.getListMessions()) {
            if (p.getStatus().equals("nv")) {
                ProduitItem(res, p);
            }
        }
        setupSideMenu(res);
    }

    private void ProduitItem(Resources res, Produit p) {

        Container produit = new Container(BoxLayout.y());
        Label nom = new Label("Nom : " + p.getNom());
      
        Label type = new Label("Type : " + p.getType());
       
        Label qte = new Label("Quantite : " + p.getGte());
      
        
        String url = "http://localhost/Bouhmid/web/images/" + p.getImg();
        System.out.println(url);
        EncodedImage enc = EncodedImage.createFromImage(res.getImage("round-mask.png"), false);
        Image profilePic = URLImage.createToStorage(enc, p.getImg(), url);
        Button plusDetail = new Button("Detail Sur Le Produit");

        plusDetail.addActionListener((evt) -> {
            Session.setProduit(p);
            new ProduitDetaillForm(res).show();
        });
        produit.addAll(nom, type,qte);
        produit.add(profilePic);
        produit.add(plusDetail);
        add(produit);
        produit.setUIID("ProduitItem");
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
}
