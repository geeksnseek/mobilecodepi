/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Component;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.layouts.Layout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;

/**
 *
 * @author debba
 */
public class BaseForm extends Form{

  //  public static Produit eq = new Produit();

    public BaseForm() {
    }

    public BaseForm(Layout contentPaneLayout) {
        super(contentPaneLayout);
    }

    public BaseForm(String title, Layout contentPaneLayout) {
        super(title, contentPaneLayout);
    }

    public Component createLineSeparator() {
        Label separator = new Label("", "WhiteSeparator");
        separator.setShowEvenIfBlank(true);
        return separator;
    }

    public Component createLineSeparator(int color) {
        Label separator = new Label("", "WhiteSeparator");
        separator.getUnselectedStyle().setBgColor(color);
        separator.getUnselectedStyle().setBgTransparency(255);
        separator.setShowEvenIfBlank(true);
        return separator;
    } 
        protected void addSideMenu(Resources res) {
        Toolbar tb = getToolbar();
        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 3) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 3);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        tb.addComponentToSideMenu(LayeredLayout.encloseIn(
                sl,
                FlowLayout.encloseCenterBottom(
                        new Label(res.getImage("profile-pic.jpg"), "PictureWhiteBackgrond"))
        ));

        tb.addMaterialCommandToSideMenu("Profile", FontImage.MATERIAL_SETTINGS, e -> new ProfileForm(res).show());
        tb.addMaterialCommandToSideMenu("Achat ", FontImage.MATERIAL_UPDATE, e -> new AchatForm(res).show());
        tb.addMaterialCommandToSideMenu("Panier", FontImage.MATERIAL_SHOPPING_BASKET, e -> new PanierForm(res).show());
        tb.addMaterialCommandToSideMenu("Associations", FontImage.MATERIAL_UPDATE, e -> new AssociationMenuForm(res).show());
//        tb.addMaterialCommandToSideMenu("Panier", FontImage.MATERIAL_SHOPPING_BASKET, e -> new PanierForm().getF().show());
       // tb.addMaterialCommandToSideMenu("Nos Produit", FontImage.MATERIAL_UPDATE, e -> new NewsfeedForm(res).show())
        tb.addMaterialCommandToSideMenu("Espace Événement",  FontImage.MATERIAL_DETAILS, e -> new EvenementMenuForm(res).show());
 //       tb.addMaterialCommandToSideMenu("Afficher Evenement", FontImage.MATERIAL_DETAILS, e -> new ShowEventsForm(res).show());
//        tb.addMaterialCommandToSideMenu("Ajouter Produit", FontImage.MATERIAL_EXIT_TO_APP, e -> new autreavis().autreavis(res));
//        tb.addMaterialCommandToSideMenu("Statistiques", FontImage.MATERIAL_DETAILS, e -> new ApiStat().createPieChartForm(res).show());
      tb.addMaterialCommandToSideMenu("Forum", FontImage.MATERIAL_CHAT_BUBBLE_OUTLINE, e -> new ForumMenuForm(res).show());
//        tb.addMaterialCommandToSideMenu("Calendrier", FontImage.MATERIAL_DETAILS, e -> new CalendarForm(res).show());

//        tb.addMaterialCommandToSideMenu("Ajout Evenement", FontImage.MATERIAL_DETAILS, e -> new HomeForm().HomeForm(res).show());
//        tb.addMaterialCommandToSideMenu("Enseigne ", FontImage.MATERIAL_DETAILS, e -> new AffichageE(res).getF().show());
//
//        tb.addMaterialCommandToSideMenu("Souk", FontImage.MATERIAL_DETAILS, e -> {
//            {
//                MapsDemo maCarte = new MapsDemo();
//                maCarte.start();
//
//            }
//        });

        tb.addMaterialCommandToSideMenu("Logout", FontImage.MATERIAL_EXIT_TO_APP, e -> new SignInForm(res).show());

    }
}
