/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Panier;
import Services.PanierService;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author brini
 */
public class ModifPanierForm 
{
          EncodedImage imc;
       Image img;
       ImageViewer imv;
        
       Form f = new Form();
    public ModifPanierForm(Panier l,Resources theme)
    {         
               String url="http://localhost/SymfonyPi/web/ImagesStock/"+l.getIdStock().getNomimage();
        
              Container cnt = new Container (new BoxLayout (BoxLayout.Y_AXIS) );  
              Container cnt1 = new Container (new BoxLayout (BoxLayout.X_AXIS) );
              Container cnt2 = new Container (new BoxLayout (BoxLayout.Y_AXIS) );
            
              Label lnom = new Label ("Produit : "+l.getIdStock().getDescription());
              lnom.getStyle().setPaddingTop(100);
              lnom.getAllStyles().setFgColor(0x228B22);
              
              Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);
              lnom.getUnselectedStyle().setFont(ttfFont);
              
              Label lqte = new Label ("Quantité disponible : "+l.getIdStock().getQuantitedispo() +" KG ");
              lqte.getStyle().setPaddingBottom(100);
              lqte.getStyle().setPaddingTop(100);
              lqte.getAllStyles().setFgColor(0x0000);
              lqte.getUnselectedStyle().setFont(ttfFont);
              
              
              Label lprix = new Label ("Prix : "+l.getPrix()+" DT / KG ");
              lprix.getAllStyles().setFgColor(0x0000);
              lprix.getUnselectedStyle().setFont(ttfFont);
              lprix.getStyle().setPaddingBottom(100);

            
              imc = EncodedImage.createFromImage(theme.getImage("high.png"), true);
            
              img=URLImage.createToStorage(imc,""+l.getIdStock().getNomimage(), url, URLImage.RESIZE_SCALE);
        
              int displayHeight = Display.getInstance().getDisplayHeight();
        
              ScaleImageLabel scaleImageLabel = new ScaleImageLabel(img);
              Image scImage = img.scaled(-1, displayHeight / 3);       
              imv= new ImageViewer(scImage);
              imv.getStyle().setPaddingLeft(250);
              
              cnt.add(lnom);
              cnt.add(imv);
              cnt.add(lqte);
              cnt.add(lprix);
//              
             Label lqtedes = new Label("Quantité desirée :"); 
             lqtedes.getStyle().setPaddingBottom(100);
             lqtedes.getAllStyles().setFgColor(0x228B22);
             lqtedes.getUnselectedStyle().setFont(ttfFont);
        
                 
            TextField tqte = new TextField("",  String.valueOf(l.getQuantite()), 20, TextField.ANY);
           

            tqte.getAllStyles().setFgColor(0x228B22);
            
             cnt2.add(lqtedes);
             cnt2.add(tqte);
            Button retoure = new Button ("Annuler");
             retoure.addActionListener(e->{
             new PanierForm(theme).show();
           });
                
    
            
            Button modifier = new Button("Modifier");
            modifier.getAllStyles().setFgColor(0xff0000);
            modifier.addActionListener(new ActionListener() {
                   @Override
                   public void actionPerformed(ActionEvent evt) {
            
                    String quantite = tqte.getText();
                     if(quantite.equals(""))
                      return;
                    Dialog ip = new InfiniteProgress().showInifiniteBlocking();
                    int quant = 0;
                    try
                    {
                    quant = Integer.parseInt(quantite);
                    }
                    catch(Exception ex)
                    {
                ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("S'il vous plait, inserez des chiffres seulement."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                return;
                 }
                if(quant >l .getIdStock().getQuantitedispo()){
                ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("Quantite depasse la limite."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                return;
                }
                else if (quant <= 0 )
                {
                ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("Quantite Négative ."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                }
                
                else 
                {
                PanierService ser = new PanierService();
//               l.setQuantite(quant);
               ser.modiflc(l.getId(),quant);
               new PanierForm(theme).show(); 
                }

                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                   }
               });
            cnt2.add(modifier);
            cnt2.add(retoure);
           f.add(cnt);
           f.add(cnt2);
           
           f.show();
    }
    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
}
