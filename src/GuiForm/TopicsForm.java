/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.UIManager;
import Entity.Topics;
import com.codename1.l10n.SimpleDateFormat;
import Services.TopicsService;
import com.codename1.contacts.Contact;
import com.codename1.ui.TextComponent;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.Resources;
import java.util.List;

public class TopicsForm {

    private Form fliste;
    Resources theme;
    private Container cnt;
    private TextComponent recherche;

    public TopicsForm() {
        theme = UIManager.initFirstTheme("/theme");

        Font fnt = Font.createSystemFont(Font.FACE_MONOSPACE, 0xB7CA79, Font.SIZE_SMALL);
        int size = Display.getInstance().convertToPixels(4);
        fliste = new Form("Topics", new BoxLayout(BoxLayout.Y_AXIS));
        fliste.getToolbar().addCommandToLeftBar("Index", FontImage.createFixed("\ue815", fnt, 0xffffff, size, size), ev -> {
            TopicsForm in = new TopicsForm();
            in.getTopic().show();
        });
                

          

            
        fliste.getToolbar().addMaterialCommandToRightBar("", FontImage.MATERIAL_ADD, (evt) -> {
            AjouterTopicForm a = new AjouterTopicForm();
            a.getAjouttopic().show();
        });
        cnt = new Container(BoxLayout.y());
        recherche = new TextComponent().hint("Rechercher");
        recherche.getField().addDataChangedListener((type, index) -> {
            rechercher(recherche.getText());
        });
        
        fliste.add(recherche);
        fliste.add(cnt);
        fliste.show();
       TopicsService es = new TopicsService();
        List<Topics> le = es.getAllTopics(null);
        for (Topics e : le) {
            cnt.add(addComponent(e));
        }
        fliste.refreshTheme();
    }
    public void rechercher(String str)
    { 
        cnt.removeAll();
        TopicsService es = new TopicsService();
        List<Topics> le = es.getAllTopics(str);
        for (Topics e : le) {
            cnt.add(addComponent(e));
        }
        fliste.refreshTheme();
        
    }

    public Container addComponent(Topics e) {
        Container cx = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container cy = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label title = new Label(e.getTitle());
        Label subj = new Label(e.getTopic_subject());
        EncodedImage enc = EncodedImage.createFromImage(Image.createImage(650, 500, 0xffffffff), true);
        String url = "http://localhost/SymfonyPi/web/imagesForum/" + e.getPhoto();
        URLImage urlimage = URLImage.createToStorage(enc, url, url);
        ImageViewer iv = new ImageViewer(urlimage);

        cy.add(title);
       // cy.add(subj);

        cx.add(iv);
        cx.add(cy);
        title.addPointerPressedListener((evt) -> {
            DetailsTopicForm mp = new DetailsTopicForm(e);
            mp.getF().show();
        });

        cx.setLeadComponent(title);
        
        
         return cx;
    }

  

    public Form getTopic() {
        return fliste;
    }

    public void setTopic(Form topic) {
        this.fliste = topic;
    }

}
