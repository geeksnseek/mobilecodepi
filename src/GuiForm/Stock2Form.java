/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Stock;
import Services.PanierService;
import Services.StockService;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import java.io.IOException;

/**
 *
 * @author brini
 */
public class Stock2Form extends BaseForm
{
    
    Form f = new Form("Voir En Detail");

    
    StockService service = new StockService();
    PanierService pservice = new PanierService();
    public Stock2Form(Resources res)
    {
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle(" Produits SMART CYCLE ");
        getContentPane().setScrollVisible(false);
     
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
        
        
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() /4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

     
        
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3, 
                            
                            FlowLayout.encloseCenter(
                                new Label(res.getImage(""), "PictureWhiteBackgrond"))
                          
                    )
                )
        ));

        Button bntinfo = new Button("Pour plus d'informations","Label");
        bntinfo.getStyle().setPaddingLeft(500);
        bntinfo.getAllStyles().setTextDecoration(Style.TEXT_DECORATION_UNDERLINE);
//        bntinfo.setTextPosition(Component.CENTER);
        bntinfo.getAllStyles().setFgColor(0xFF0000);
                      
        Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);
        bntinfo.getUnselectedStyle().setFont(ttfFont);
        bntinfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               new MailStockForm(res).show();
            }
        });
        
          addStringValue("", bntinfo);
       for (Stock s : service.getList1())
      {
        try 
        {
            addStringValue("", addItem(s,res));

        } 
        catch (IOException ex)
        {
                    
        }
       }
 
   
    }
    
        private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    } 
  public Container addItem(Stock s,Resources theme) throws IOException 
  {
        Image icon1 = theme.getImage("back4.png");  
       UIBuilder  ui = new UIBuilder();
       Container parent =new Container(BoxLayout.x());
       Form f1 = new Form(); 
        
       EncodedImage imc;
       Image img;
       ImageViewer imv;
       String url="http://localhost/SymfonyPi/web/ImagesStock/"+s.getNomimage();
       Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
    
        Label  lnom = new Label(s.getDescription());  
        lnom.getStyle().setPaddingBottom(100);
        lnom.getStyle().setPaddingTop(100);
        lnom.getAllStyles().setFgColor(0x228B22);
              
        Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);
        lnom.getUnselectedStyle().setFont(ttfFont);
        
        
       Button btndetail = new Button("Voir En Detail","Label");
        btndetail.getStyle().setPaddingLeft(100);
        btndetail.getStyle().setPaddingTop(150);
        btndetail.getAllStyles().setTextDecoration(Style.TEXT_DECORATION_UNDERLINE);
        btndetail.setTextPosition(Component.RIGHT);
        btndetail.getAllStyles().setFgColor(0x228B22);
        
        
        C1.add(lnom);  
        imc = EncodedImage.createFromImage(theme.getImage("vol.jpg"), false);
            
        img=URLImage.createToStorage(imc,""+s.getNomimage(), url, URLImage.RESIZE_SCALE);
        
        int displayHeight = Display.getInstance().getDisplayHeight();
        
        ScaleImageLabel scaleImageLabel = new ScaleImageLabel(img);
        Image scImage = img.scaled(-1, displayHeight / 5);       
        imv= new ImageViewer(scImage);
        C1.add(imv);
        parent.add(C1);
        parent.add(btndetail);

   
        btndetail.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt)
            {

              Container cnt = new Container (new BoxLayout (BoxLayout.Y_AXIS) );  
              Container cnt1 = new Container (new BoxLayout (BoxLayout.X_AXIS) );
              Container cnt2 = new Container (new BoxLayout (BoxLayout.Y_AXIS) );
            
              Label lnom = new Label ("Produit : "+s .getDescription());
              lnom.getStyle().setPaddingTop(100);
              lnom.getAllStyles().setFgColor(0x228B22);
              
              Font ttfFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_LARGE);
              lnom.getUnselectedStyle().setFont(ttfFont);
              
              Label lqte = new Label ("Quantité disponible : "+s .getQuantitedispo() +" KG ");
              lqte.getStyle().setPaddingBottom(100);
              lqte.getStyle().setPaddingTop(100);
              lqte.getAllStyles().setFgColor(0x0000);
              lqte.getUnselectedStyle().setFont(ttfFont);
              
              
              Label lprix = new Label ("Prix : "+s.getPrixuni()+" DT / KG ");
              lprix.getAllStyles().setFgColor(0x0000);
              lprix.getUnselectedStyle().setFont(ttfFont);
              lprix.getStyle().setPaddingBottom(100);

            
              int displayHeight = Display.getInstance().getDisplayHeight();
              ScaleImageLabel scaleImageLabel = new ScaleImageLabel(img);
              Image scImage1 = img.scaled(-1, displayHeight /3);
              ImageViewer imvo ;
              imvo= new ImageViewer (scImage1);
              imvo.getStyle().setPaddingTop(100);
              
              cnt.add(lnom);
              cnt.add(imvo);
              cnt.add(lqte);
              cnt.add(lprix);
//              
             Label lqtedes = new Label("Quantité desirée :"); 
             lqtedes.getStyle().setPaddingBottom(100);
             lqtedes.getAllStyles().setFgColor(0x228B22);
             lqtedes.getUnselectedStyle().setFont(ttfFont);
        
                 
            TextField tqte = new TextField("", "Saisair la quantité", 20, TextField.ANY);
            tqte.getAllStyles().setFgColor(0x228B22);
             Button panier= new Button("Ajouter au panier");
             
             panier.addActionListener(new ActionListener()
             {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                  if(tqte.getText().equals(""))
                      return;
                    Dialog ip = new InfiniteProgress().showInifiniteBlocking();
                    int quant = 0;
                    try
                    {
                    quant =  Integer.parseInt(tqte.getText());
                    }
                    catch(Exception ex)
                    {
                ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("S'il vous plait, inserez des chiffres seulement."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                return;
                 }
                     if(quant >s.getQuantitedispo()){
                ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("Quantite depasse la limite."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                return;
                }
                     else if (quant <= 0 )
                {
                ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("Quantite Négative ."));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                }
                       else 
                {
                PanierService ser = new PanierService();
//               l.setQuantite(quant);
                pservice.ajouterlc(s.getIdCat(),quant );
                   Dialog.show("Confirmation", "Ajout effectué", "Ok", null);
               new Stock2Form(theme).show(); 
                }
          

                
                
                
            } 
             });
             
              f.getToolbar().addCommandToLeftBar("back",icon1,new ActionListener<ActionEvent>() {
            @Override
            public void actionPerformed(ActionEvent evt) {
             Stock2Form produit=new Stock2Form(theme);
                produit.show();
            }
        });
     
             cnt2.add(lqtedes);
             cnt2.add(tqte);
             cnt2.add(panier);
   
             
           f.add(cnt);
           f.add(cnt2);

        
           f.show();


            }

        });
               
                   
 
               
        return parent ;    
               
    }
 
}
