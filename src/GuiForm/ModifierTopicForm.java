/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Topics;
import Services.TopicsService;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;

/**
 *
 * @author nawre
 */
public class ModifierTopicForm {

    private Button modifier, annuler;
    private TextField subj, titre;
    private Container cntl, cnt2;
    Font fnt = Font.createSystemFont(Font.FACE_MONOSPACE, 0xB7CA79, Font.SIZE_SMALL);
    int size = Display.getInstance().convertToPixels(4);
    Form hi = new Form("Modifier Topic", new BoxLayout(BoxLayout.Y_AXIS));

    public ModifierTopicForm(Topics t) {
        hi = new Form("Modifier Topic", new FlowLayout(Container.CENTER, Container.CENTER));
        hi.getToolbar().addMaterialCommandToLeftBar("", FontImage.MATERIAL_ARROW_BACK, (evt) -> {
            DetailsTopicForm a = new DetailsTopicForm(t);
            a.getF().show();
        });
        titre = new TextField(t.getTitle());
        subj = new TextField(t.getTopic_subject());

        cntl = new Container(BoxLayout.y());
        cnt2 = new Container(BoxLayout.x());

        modifier = new Button("Modifier");
        annuler = new Button("Annuler");

        modifier.addActionListener((evt) -> {

            boolean bool = Dialog.show("Modifier", "Voulez vous vraiment modifier topic :" + t.getTitle(), "OUI", "NON");
           

                if (bool) {
                    TopicsService os = new TopicsService();
                    // System.out.println(t.getTopic_id());
                     if ((titre.getText() != "") && (subj.getText() != "")) {
                    t.setTopic_title(titre.getText());
                    t.setTopic_subject(subj.getText());
                    os.ModiferTopic(t.getTopic_id(), t);
                    TopicsForm mp = new TopicsForm();
                    mp.getTopic().show();
                    Dialog.show("Modification", "Modification affectuée avec succès", "Ok", null);
                } else {
                      Dialog.show("Alert", "Veuillez saisir tout les champs SVP !", "ok", null);

                   
                }
            } else {
                     ModifierTopicForm mp = new ModifierTopicForm(t);
                    mp.getF().showBack();

            }
        });
        annuler.addActionListener((evt) -> {
            TopicsForm mt = new TopicsForm();
            mt.getTopic().showBack();
        });

        cntl.add(titre);
        cntl.add(subj);
        cnt2.add(modifier);
        cnt2.add(annuler);
        cntl.add(cnt2);

        hi.add(cntl);
    }

    public Form getF() {
        return hi;
    }

    public void setF(Form hi) {
        this.hi = hi;
    }

}
