/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Association;
import Entity.Demande;
import Services.MembreService;
import Services.ProfileClientServices;
import Services.ServiceAssociation;
import com.codename1.components.ImageViewer;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import java.util.Date;
import rest.file.uploader.tn.FileUploader;

/**
 *
 * @author med
 */
public class MyAssociationForm extends BaseForm {

    ServiceAssociation sa = new ServiceAssociation();
    String path;
    ProfileClientServices pf = new ProfileClientServices();
    Form detailsForm;

    public MyAssociationForm(Resources res) {

        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Mes association");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                        GridLayout.encloseIn(3, FlowLayout.encloseCenter(new Label(res.getImage(""), "PictureWhiteBackgrond")))
                )
        ));
        for (Association a : sa.getListMyAssociations()) {
            addStringValue("", AddItem(a, res));
        }

    }

    public Container AddItem(Association a, Resources theme) {
        ConnectionRequest con = new ConnectionRequest();
        Container parent = new Container(BoxLayout.x());
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label lnom = new Label(a.getNomAssociation());
        String strd = "1" + a.getDateCreation().substring(114, 123) + "000";
        long timestamp = Long.parseLong(strd);
        Date d = new Date(timestamp);
        SpanLabel ldate = new SpanLabel("Depuis le :" + d.toString());
        Button btacc = new Button("Modifier");
        Button btsup = new Button("Supprimer");

        C1.add(lnom);
        C1.add(ldate);
        C1.add(btacc);
        C1.add(btsup);
        String url = "http://localhost:3309/SymfonyPi/web/images/" + a.getNomImage();
        EncodedImage enc = EncodedImage.createFromImage(theme.getImage("eco_nuevo.png"), false);
        Image image = URLImage.createToStorage(enc, "img" + a.getNomAssociation(), url, URLImage.RESIZE_SCALE);
        EncodedImage enc2 = EncodedImage.createFromImage(theme.getImage("eco_nuevo.png"), false);
        Image image2 = URLImage.createToStorage(enc2, "img" + a.getNomAssociation(), url);

        ImageViewer img = new ImageViewer(image);
        ImageViewer img2 = new ImageViewer(image2);
        parent.add(img);
        parent.add(C1);

        btacc.addActionListener(evt -> {
            ServiceAssociation sa = new ServiceAssociation();
            TextField tnom = new TextField(a.getNomAssociation(), "nom");
            TextField tadresse = new TextField(a.getAdresse(), "adresse");
            TextField tcapital = new TextField(a.getCapital()+"", "capital");
            tnom.setUIID("TextFieldBlack");
            tadresse.setUIID("TextFieldBlack");
            tcapital.setUIID("TextFieldBlack");

            detailsForm = new Form("Modifier Asso", BoxLayout.y());
            SpanLabel welcome = new SpanLabel("Modifier les donner de l'association " + a.getNomAssociation());

            Button btretour = new Button("retour");
            Button btupd = new Button("modifier");
            
            detailsForm.add(welcome);
            detailsForm.add(tnom);
            detailsForm.add(tadresse);
            detailsForm.add(tcapital);
            detailsForm.add(btupd);
            detailsForm.add(btretour);
            detailsForm.show();
            btupd.addActionListener(up -> {
             
    
    if(!tnom.getText().isEmpty()&&!tadresse.getText().isEmpty()&&!tcapital.getText().isEmpty()){
               if(Integer.parseInt(tcapital.getText())>=0){
                   a.setCapital(Integer.parseInt(tcapital.getText()));
                   a.setNomAssociation(tnom.getText());
                   a.setAdresse(tadresse.getText());
                  sa.updateAsso(a);
                Dialog.show("Info", "informations modifier !", "ok", null);
               }else Dialog.show("Erreur", "le capital doit etre positif !", "ok", null);
           }else Dialog.show("Erreur", "Champ vide !", "ok", null);
            });
            btretour.addActionListener(re -> {

                MyAssociationForm l = new MyAssociationForm(theme);
                l.show();
            });
        });
        btsup.addActionListener(e -> {
            sa.deleteAsso(a);
            Dialog.show("Info", "Association supprimer !", "ok", null);
        });

        return parent;
    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
