/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Event;
import Entity.Participant;
import Services.EventServices;
import Services.MembreService;
import static com.codename1.charts.util.ColorUtil.CYAN;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;

/**
 *
 * @author debba
 */
public class CodeCadeauEventForm {
public static String url = "http://localhost/SymfonyPi/web/imagesEvent";
    Form f;
    TextField tavis;
    Label tavis1;
    Button btnMod, btnAnnuler;
    static String k;
   static String code;
 static   ImageViewer codeimg;

    public CodeCadeauEventForm(Resources res)  {

        f = new Form("CodeCadeau", BoxLayout.y());

        Container c2 = new Container(BoxLayout.x());
        tavis1 = new Label("Code Cadeau:");
        tavis = new TextField("", "Tapez votre code cadeau ici", 20, TextField.ANY);
        tavis.getAllStyles().setFgColor(CYAN);
        tavis1.getAllStyles().setFgColor(0xA83839);
        c2.addAll(tavis1, tavis);
        

        btnMod = new Button("Envoyer");
        btnAnnuler = new Button("Annuler");
        f.addAll(c2);
        f.add(btnMod);
        f.add(btnAnnuler);
        ImageViewer imv=new ImageViewer();
        ImageViewer codeimg = new ImageViewer();
       Image codeQR = res.getImage("CodeQr.jpg");
     //   codeQR.scaled(1, 1);
                  ImageViewer imgV = new ImageViewer();
                Image placeholder = Image.createImage(50,50);
                EncodedImage enc = EncodedImage.createFromImage(placeholder, false);
                URLImage urlim = URLImage.createToStorage(enc, url + "/" + "CodeQr.jpg", url + "/" + "CodeQr.jpg");
               
    Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
    int displayHeight = Display.getInstance().getDisplayHeight();
          ScaleImageLabel scaleImageLabel = new ScaleImageLabel(codeQR);
        Image scImage = codeQR.scaled(-1, displayHeight / 5);       
     
       
        C1.add(imv);
     //  codeimg.setHidden(true);
        //f.add(codeimg);
        f.add(C1);
        
        btnMod.addActionListener((e) -> {

            //    ta.setAvis(tavis.getText());
            EventServices ES = new EventServices();
         //   ArrayList<Participant> listp = ES.ChercherParticipant(ta.getId(), MembreService.user.getId());
         //   Participant parti = listp.get(0);
        //    parti.setAvis(tavis.getText());
       //     k = parti.getAvis();
            System.out.println(k);
            System.out.println("9bal donner code cadeau");
         //   ES.DonnerAvisEvent(ta, k);
 code=tavis.getText();
  System.out.println(code);
 if( ES.VerifCode(code).get(0) == 1)
 { 
ES.CodeCadeauEvent(code);

 imv.setImage(scImage);
 System.out.println(scImage);           
 System.out.println("baad donner code cadeau");
  //Dialog.show("Succès", "Code correcrte! Voici ton codeQR cadeau ","ok", null);
                Dialog ip = new InfiniteProgress().showInifiniteBlocking();
                 ip.dispose();
                InteractionDialog dlg = new InteractionDialog("Notification");
                dlg.setLayout(new BorderLayout());
                dlg.add(BorderLayout.CENTER, new SpanLabel("Succès,Code correcrte,Voici ton codeQR cadeau"));
                Button close = new Button("Close");
                close.addActionListener((ee) -> dlg.dispose());
                dlg.addComponent(BorderLayout.SOUTH, close);
                Dimension pre = dlg.getContentPane().getPreferredSize();
                dlg.show(50, 100, 30, 30);
                return;
 }else{
  Dialog.show("erreur", "code erroné!! veuillez entrer un code valide", "Ok", null);
 }

        });

        btnAnnuler.addActionListener((e) -> {
            ShowMyParticipationForm a = new ShowMyParticipationForm(res);
            a.show();
        });
        f.getToolbar().addCommandToLeftBar("", res.getImage("retourner.png"), eve -> {
            ShowMyParticipationForm h = new ShowMyParticipationForm(res);
            h.show();
        });
        f.show();
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
   
}

