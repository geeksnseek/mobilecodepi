/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Association;
import Entity.Demande;
import Services.MembreService;
import Services.ProfileClientServices;
import Services.ServiceAssociation;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.MediaPlayer;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ShareButton;
import com.codename1.components.SpanLabel;
import com.codename1.facebook.ui.LikeButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.ComponentGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import rest.file.uploader.tn.FileUploader;

/**
 *
 * @author med
 */
public class ListAssociationForm extends BaseForm {

    ServiceAssociation sa = new ServiceAssociation();
    String path;
    ProfileClientServices pf = new ProfileClientServices();
    Form detailsForm;
    TextField rdesc;

    public ListAssociationForm(Resources res) {
        
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Tous les associations");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });
        

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                        GridLayout.encloseIn(3, FlowLayout.encloseCenter(new Label(res.getImage(""), "PictureWhiteBackgrond")))
                )
        ));
         for (Association a : sa.getList2())
      {
          addStringValue("", AddItem(a,res));
       }
         
         

    }

    public Container AddItem(Association a,Resources theme) {
        ConnectionRequest con = new ConnectionRequest();
        Container parent = new Container(BoxLayout.x());
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label lnom = new Label(a.getNomAssociation());
        String strd = "1"+a.getDateCreation().substring(114,123)+"000";
        long timestamp = Long.parseLong(strd);
        Date d = new Date(timestamp );
        SpanLabel ldate = new SpanLabel("Depuis le :"+d.toString());
        Button btacc = new Button(sa.getButtonName(a.getIdAssociation()));
        C1.add(lnom);
        C1.add(ldate);
        C1.add(btacc);
        C1.setLeadComponent(btacc);
        String url = "http://localhost:3309/SymfonyPi/web/images/" + a.getNomImage();
        EncodedImage enc = EncodedImage.createFromImage(theme.getImage("eco_nuevo.png"), false);
        Image image = URLImage.createToStorage(enc,"img"+a.getNomAssociation(), url, URLImage.RESIZE_SCALE);
        EncodedImage enc2 = EncodedImage.createFromImage(theme.getImage("eco_nuevo.png"), false);
        Image image2 = URLImage.createToStorage(enc2,"img"+a.getNomAssociation(), url);
        

        ImageViewer img= new ImageViewer(image);
        ImageViewer img2= new ImageViewer(image2);
        parent.add(img);
        parent.add(C1);
        
        btacc.addActionListener(evt->{
            Dialog ip = new InfiniteProgress().showInifiniteBlocking();
            ServiceAssociation sa = new ServiceAssociation();
            Demande demande = new Demande(a.getIdAssociation(), MembreService.user.getId());
            sa.ajoutAdheration(demande);
            Label lrate = new Label("voter !");
            rdesc = new TextField("","exprimer vous !");
            Slider SN = createStarRankSlider();

            detailsForm = new Form("details",BoxLayout.y());
            SpanLabel welcome = new SpanLabel("Bienvenue a l'association "+a.getNomAssociation());
            Button btvote = new Button("Voter ");
            btvote.addActionListener(e->{
                float f = (float) SN.getProgress();
                float f2 = f/2;
                sa.ajoutVote(a.getIdAssociation(), f2, rdesc.getText());
                Dialog.show("Info", "Votre vote a été enregistrer!", "ok", null);
                
            });
            Button btretour = new Button("retour");
            btretour.addActionListener(re->{
                Dialog ip2 = new InfiniteProgress().showInifiniteBlocking();
                ListAssociationForm l = new ListAssociationForm(theme);
                l.show();
            });
            SN.addActionListener(e->{
                float f = (float) SN.getProgress();
                float f2 = f/2;
                System.out.println(f2);
                 btvote.setText("Voter "+f2 );
            });
            detailsForm.add(welcome);
            
            detailsForm.add(img2);
            
            detailsForm.addAll(FlowLayout.encloseCenter(lrate),FlowLayout.encloseCenter(SN));
            detailsForm.addAll(rdesc,btvote);
            detailsForm.add(FlowLayout.encloseCenter(createDemo(detailsForm)));
            detailsForm.add(createMail());
            detailsForm.add(btretour);
            detailsForm.add(createVplayer());     
            detailsForm.show();
        });

        return parent;
    }
    
    private void initStarRankStyle(Style s, Image star) {
    s.setBackgroundType(Style.BACKGROUND_IMAGE_TILE_BOTH);
    s.setBorder(Border.createEmpty());
    s.setBgImage(star);
    s.setBgTransparency(0);
}

private Slider createStarRankSlider() {
    Slider starRank = new Slider();
    starRank.setEditable(true);
    starRank.setMinValue(0);
    starRank.setMaxValue(10);
    Font fnt = Font.createTrueTypeFont("native:MainLight", "native:MainLight").
            derive(Display.getInstance().convertToPixels(5, true), Font.STYLE_PLAIN);
    Style s = new Style(0xffff33, 0, fnt, (byte)0);
    Image fullStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
    s.setOpacity(100);
    s.setFgColor(0);
    Image emptyStar = FontImage.createMaterial(FontImage.MATERIAL_STAR, s).toImage();
    initStarRankStyle(starRank.getSliderEmptySelectedStyle(), emptyStar);
    initStarRankStyle(starRank.getSliderEmptyUnselectedStyle(), emptyStar);
    initStarRankStyle(starRank.getSliderFullSelectedStyle(), fullStar);
    initStarRankStyle(starRank.getSliderFullUnselectedStyle(), fullStar);
    starRank.setPreferredSize(new Dimension(fullStar.getWidth() * 5, fullStar.getHeight()));
    return starRank;
}
public Container createDemo(final Form parentForm) {
        Container social = new Container(new BoxLayout(BoxLayout.X_AXIS));
        LikeButton b = new LikeButton();
        social.addComponent(b);
        ShareButton s = new ShareButton();
        s.setText("Share");
        s.setTextToShare("Codename One is so COOL!!!");
        social.addComponent(s);
        //b.setUIID("Label");
        return social;
    }
public Container createVplayer() {
        Container player = new Container(new BorderLayout());
        final MediaPlayer mp = new MediaPlayer();
        try {
            
            InputStream is = Display.getInstance().getResourceAsStream(getClass(), "/video.mp4");
            if(is != null) {
                mp.setDataSource(is, "video/mp4", null);
            } else {
                mp.setDataSource("https://dl.dropbox.com/u/57067724/cn1/video.mp4");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        player.addComponent(BorderLayout.CENTER, mp);
        return player;
    }
 public Container createMail() {
        Container message = new Container(new BoxLayout(BoxLayout.Y_AXIS));        
        ComponentGroup gp = new ComponentGroup();
        message.addComponent(gp);
        
        final TextField to = new TextField("SMARTCYCLE@esprit.tn");
        to.setUIID("TextFieldBlack");
        to.setHint("TO:");
        gp.addComponent(to);

        final TextField subject = new TextField("Contact SMARTCYCLE");
        subject.setHint("Subject");
        subject.setUIID("TextFieldBlack");
        gp.addComponent(subject);
        final TextField body = new TextField("how we can improve your experience");
        body.setSingleLineTextArea(false);
        body.setRows(4);
        body.setUIID("TextFieldBlack");
        body.setHint("Message Body");
        gp.addComponent(body);
        
        ComponentGroup buttonGroup = new ComponentGroup();
        Button btn = new Button("Send");
        buttonGroup.addComponent(btn);
        message.addComponent(buttonGroup);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                Message msg = new Message(body.getText());
                Display.getInstance().sendMessage(new String[] {to.getText()}, subject.getText(), msg);
            }
        });
                
        return message;
    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
