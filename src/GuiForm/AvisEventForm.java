/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Event;
import Entity.Participant;
import Services.EventServices;
import Services.MembreService;
import static com.codename1.charts.util.ColorUtil.CYAN;
import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;

/**
 *
 * @author debba
 */
public class AvisEventForm {

    Form f;
    TextField tavis;
    Label tavis1;
    Button btnMod, btnAnnuler;
    static String k;
    

    public AvisEventForm(Event ta, Resources res)  {
        
        f = new Form("Avis", BoxLayout.y());

        Container c2 = new Container(BoxLayout.x());
        tavis1 = new Label("Avis:");
        tavis = new TextField("", "Tapez votre avis ici", 20, TextField.ANY);
        tavis.getAllStyles().setFgColor(CYAN);
        tavis1.getAllStyles().setFgColor(0xA83839);
        c2.addAll(tavis1, tavis);
        

        btnMod = new Button("Envoyer");
        btnAnnuler = new Button("Annuler");
        f.addAll(c2);
        f.add(btnMod);
        f.add(btnAnnuler);
        btnMod.addActionListener((e) -> {

            //    ta.setAvis(tavis.getText());
            EventServices ES = new EventServices();
            ArrayList<Participant> listp = ES.ChercherParticipant(ta.getId(), MembreService.user.getId());
            Participant parti = listp.get(0);
            parti.setAvis(tavis.getText());
            k = parti.getAvis();
            System.out.println(k);
            System.out.println("9bal donner avis");
            ES.DonnerAvisEvent(ta, k);

            System.out.println("baad donner avis");


        });

        btnAnnuler.addActionListener((e) -> {
            ShowMyParticipationForm a = new ShowMyParticipationForm(res);
            a.show();
        });
        f.getToolbar().addCommandToLeftBar("", res.getImage("retourner.png"), eve -> {
            ShowMyParticipationForm h = new ShowMyParticipationForm(res);
            h.show();
        });
        f.show();
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
   
}
