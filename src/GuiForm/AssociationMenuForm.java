/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Association;
import Services.MembreService;
import Services.ProfileClientServices;
import Services.ServiceAssociation;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.ScaleImageLabel;
import com.codename1.io.FileSystemStorage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import rest.file.uploader.tn.FileUploader;


/**
 *
 * @author med
 */
public class AssociationMenuForm extends BaseForm{

   public static TextField tnom,tadresse,tcapital,image;
   Button btnajout,btnaff,btnmanage,btnadh;
    String path;
   ProfileClientServices pf = new ProfileClientServices();
    

    public AssociationMenuForm(Resources res) {
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Espace associations");
        getContentPane().setScrollVisible(false);
        
        image = new TextField();
        tnom = new TextField("","nom");
        tadresse = new TextField("","adresse");
        tcapital = new TextField("","capital");
        btnajout = new Button("Ajouter association");
        btnaff= new Button("Tous les associations");
        btnmanage= new Button("gerer mes associations");
        btnadh= new Button("voir mes adhérations");
        
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
  
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3,FlowLayout.encloseCenter(new Label(res.getImage(""), "PictureWhiteBackgrond"))     )
                )
        ));

                    btnajout.addActionListener((e) -> {
                        Dialog ip = new InfiniteProgress().showInifiniteBlocking();
                        new AddAssociationForm(res).show();
                 
        });
      
        btnaff.addActionListener((e)->{
            Dialog ip = new InfiniteProgress().showInifiniteBlocking();
            new ListAssociationForm(res).show();
        });
        
        
        btnmanage.addActionListener(e -> {
            Dialog ip = new InfiniteProgress().showInifiniteBlocking();
           new MyAssociationForm(res).show();
        });
        btnadh.addActionListener(e -> {
            Dialog ip = new InfiniteProgress().showInifiniteBlocking();
           new ListAdherationForm(res).show();
        });
    

        
        addStringValue("=>", btnaff);
        addStringValue("=>", btnadh);
        addStringValue("=>", btnajout);
        addStringValue("=>", btnmanage);
        
        
     

    
}

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
