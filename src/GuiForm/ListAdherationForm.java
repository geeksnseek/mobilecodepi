/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Association;
import Entity.Demande;
import Services.MembreService;
import Services.ProfileClientServices;
import Services.ServiceAssociation;
import com.codename1.components.ImageViewer;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.FileSystemStorage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import java.util.Date;
import rest.file.uploader.tn.FileUploader;

/**
 *
 * @author med
 */
public class ListAdherationForm extends BaseForm {

    ServiceAssociation sa = new ServiceAssociation();
    String path;
    ProfileClientServices pf = new ProfileClientServices();
    Form detailsForm;

    public ListAdherationForm(Resources res) {
        
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Mes Adhérations ");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });
        

        Image img = res.getImage("profile-background.jpg");
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                        GridLayout.encloseIn(3, FlowLayout.encloseCenter(new Label(res.getImage(""), "PictureWhiteBackgrond")))
                )
        ));
         for (Association a : sa.getListadh())
      {
          addStringValue("", AddItem(a,res));
       }

    }

    public Container AddItem(Association a,Resources theme) {
        ConnectionRequest con = new ConnectionRequest();
        Container parent = new Container(BoxLayout.x());
        Container C1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label lnom = new Label(a.getNomAssociation());
        String strd = "1"+a.getDateCreation().substring(114,123)+"000";
        long timestamp = Long.parseLong(strd);
        Date d = new Date(timestamp );
        SpanLabel ldate = new SpanLabel("Depuis le :"+d.toString());
        Button btacc = new Button("Accéder");
        C1.add(lnom);
        C1.add(ldate);
        C1.add(btacc);
        C1.setLeadComponent(btacc);
        String url = "http://localhost:3309/SymfonyPi/web/images/" + a.getNomImage();
        EncodedImage enc = EncodedImage.createFromImage(theme.getImage("eco_nuevo.png"), false);
        Image image = URLImage.createToStorage(enc,"img"+a.getNomAssociation(), url, URLImage.RESIZE_SCALE);
        EncodedImage enc2 = EncodedImage.createFromImage(theme.getImage("eco_nuevo.png"), false);
        Image image2 = URLImage.createToStorage(enc2,"img"+a.getNomAssociation(), url);
        

        ImageViewer img= new ImageViewer(image);
        ImageViewer img2= new ImageViewer(image2);
        parent.add(img);
        parent.add(C1);
        
        btacc.addActionListener(evt->{
            ServiceAssociation sa = new ServiceAssociation();
            Demande demande = new Demande(a.getIdAssociation(), MembreService.user.getId());
            sa.ajoutAdheration(demande);
            
            detailsForm = new Form("details",BoxLayout.y());
            SpanLabel welcome = new SpanLabel("Bienvenue a l'association "+a.getNomAssociation());
            
            Button btretour = new Button("retour");
            btretour.addActionListener(re->{
                
                ListAdherationForm l = new ListAdherationForm(theme);
                l.show();
            });
            detailsForm.add(welcome);
            detailsForm.add(img2);
            detailsForm.add(btretour);
            detailsForm.show();
        });

        return parent;
    }

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
