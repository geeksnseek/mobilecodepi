/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GuiForm;

import Entity.Association;
import Services.MembreService;
import Services.ProfileClientServices;
import Services.ServiceAssociation;
import com.codename1.components.ScaleImageLabel;
import com.codename1.io.FileSystemStorage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;
import rest.file.uploader.tn.FileUploader;


/**
 *
 * @author med
 */
public class AddAssociationForm extends BaseForm{

   public static TextField tnom,tadresse,tcapital,image;
   Button btnajout,btnaff,btnupload;
    String path;
   ProfileClientServices pf = new ProfileClientServices();
    

    public AddAssociationForm(Resources res) {
        super("", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Ajouter une association");
        getContentPane().setScrollVisible(false);
        
        image = new TextField();
        tnom = new TextField("","nom");
        tadresse = new TextField("","adresse");
        tcapital = new TextField("","capital");
        btnajout = new Button("ajouter");
        btnaff=new Button("Affichage");
        btnupload = new Button("parcourir image");
        
        super.addSideMenu(res);
        
        tb.addSearchCommand(e -> {});
        tb.addCommandToLeftBar("", res.getImage("left_arrow_50.png"), eve -> {    
            });
  
        Image img = res.getImage("profile-background.jpg");
        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 4) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 4);
        }
        ScaleImageLabel sl = new ScaleImageLabel(img);
        sl.setUIID("BottomPad");
        sl.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        add(LayeredLayout.encloseIn(
                sl,
                BorderLayout.south(
                    GridLayout.encloseIn(3,FlowLayout.encloseCenter(new Label(res.getImage(""), "PictureWhiteBackgrond"))     )
                )
        ));

        tnom.setUIID("TextFieldBlack");
//        tnom.getStyle().setPaddingBottom(20);
        addStringValue("nom d'association", tnom);

        tadresse.setUIID("TextFieldBlack");
        addStringValue("adresse", tadresse);

        tcapital.setUIID("TextFieldBlack");
        addStringValue("capital", tcapital); 
        
//          Validator val = new Validator();
//        
//                val.addConstraint(tnom, new LengthConstraint(4));
//                String testname="^\\(?([a-z]{3})\\)?";
//               val.addConstraint(tnom, new RegexConstraint(testname, ""));
//               val.addConstraint(tadresse, new LengthConstraint(4));
//                String testadresse="^\\(?([a-z]{3})\\)?";
//               val.addConstraint(tnom, new RegexConstraint(testadresse, ""));
//               val.addConstraint(tcapital, new LengthConstraint(4));
//                String testcapital="^\\(?([1-9]{3})\\)?";
//               val.addConstraint(tcapital, new RegexConstraint(testcapital, ""));
               
                    btnajout.addActionListener((e) -> {
            ServiceAssociation ser = new ServiceAssociation();
            
            FileUploader fc = new FileUploader("localhost:3309/SymfonyPi/web/images");
            System.out.println("owel el try");
            try {
                System.out.println("owel el try");
                String f = fc.upload(image.getText());
                image.setText(f);
                //String f = fc.upload("C:/Users/med/Desktop/i/coca.jpg");
                System.out.println("ba3d el upload ma3 : "+ f);
               
            } catch (Exception ex) {
            }
            Association a = new Association(0, tnom.getText(), tadresse.getText(),Double.parseDouble(tcapital.getText()),MembreService.user.getId(),image.getText());
                        System.out.println(MembreService.user.getId());
           try {  
    
    if(!tnom.getText().isEmpty()&&!tadresse.getText().isEmpty()&&!tcapital.getText().isEmpty()){
               if(Integer.parseInt(tcapital.getText())>=0){
                   ser.ajoutAsso(a);
           Dialog.show("Info", "Association ajouter !", "ok", null);
               }else Dialog.show("Erreur", "le capital doit etre positif !", "ok", null);
           }else Dialog.show("Erreur", "Champ vide !", "ok", null);
    
  } catch(NumberFormatException ex){  
     
  }  
               
           
            
        });
//        
        btnaff.addActionListener((e)->{
//        ListAssociationForm a=new ListAssociationForm(res);
//        a.getF().show();
        });
        
        
        btnupload.addActionListener(e -> {
            Display.getInstance().openGallery(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    if (ev != null && ev.getSource() != null) {
                        path = (String) ev.getSource();
                        System.out.println("hetha l path :"+path.substring(7) + "w hetha :"+path);
                        Image img = null;
                        image.setText(path.substring(7));//image heya just label nsob feha fel path
                        try {
                            img = Image.createImage(FileSystemStorage.getInstance().openInputStream(path));
                            System.out.println(img);
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }, Display.GALLERY_IMAGE);
        });
    

        addStringValue("", btnupload);
     
        addStringValue("", btnajout);
     

    
}

    private void addStringValue(String s, Component v) {
        add(BorderLayout.west(new Label(s, "PaddedLabel")).
                add(BorderLayout.CENTER, v));
        add(createLineSeparator(0xeeeeee));
    }
}
